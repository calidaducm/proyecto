-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-07-2015 a las 20:04:57
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `auditoria`
--
CREATE DATABASE IF NOT EXISTS `auditoria` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `auditoria`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `Rut` varchar(20) NOT NULL,
  `Nombres` varchar(100) NOT NULL,
  `Apellidos` varchar(100) NOT NULL,
  `Año_ingreso` date NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Telefono` varchar(11) NOT NULL,
  `Estado` varchar(5) NOT NULL COMMENT 'Habilitado o no',
  PRIMARY KEY (`Rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`Rut`, `Nombres`, `Apellidos`, `Año_ingreso`, `Direccion`, `Email`, `Telefono`, `Estado`) VALUES
('17447770', 'Maydelin', 'Gonzalez', '0000-00-00', 'Villa Alegre', 'may.strokes@gmail.com', '72118333', '0'),
('17810476', 'Belen', 'Vasquez', '0000-00-00', 'Talca', 'belen.vasquez@gmail.com', '61999928', '0'),
('17967786', 'Juan', 'Espinosa', '0000-00-00', 'Talca', 'juanespinosa@gmail.com', '12345678', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnoasignaturas`
--

CREATE TABLE IF NOT EXISTS `alumnoasignaturas` (
  `Rut_alumno` varchar(20) NOT NULL,
  `Codigo_asignatura` varchar(20) NOT NULL,
  `Estado` varchar(20) NOT NULL COMMENT 'aprobado o no',
  PRIMARY KEY (`Rut_alumno`,`Codigo_asignatura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumnoasignaturas`
--

INSERT INTO `alumnoasignaturas` (`Rut_alumno`, `Codigo_asignatura`, `Estado`) VALUES
('17447770', '1', 'Aprobado'),
('17447770', '10', 'Aprobado'),
('17447770', '11', 'Aprobado'),
('17447770', '12', 'Aprobado'),
('17447770', '13', 'Aprobado'),
('17447770', '14', 'Aprobado'),
('17447770', '15', 'Aprobado'),
('17447770', '16', 'Aprobado'),
('17447770', '17', 'Aprobado'),
('17447770', '18', 'Aprobado'),
('17447770', '19', 'Aprobado'),
('17447770', '2', 'Aprobado'),
('17447770', '20', 'Aprobado'),
('17447770', '21', 'No Aprobado'),
('17447770', '22', 'No Aprobado'),
('17447770', '23', 'No Aprobado'),
('17447770', '24', 'No Aprobado'),
('17447770', '25', 'No Aprobado'),
('17447770', '26', 'No Aprobado'),
('17447770', '27', 'No Aprobado'),
('17447770', '28', 'No Aprobado'),
('17447770', '29', 'No Aprobado'),
('17447770', '3', 'Aprobado'),
('17447770', '30', 'No Aprobado'),
('17447770', '31', 'No Aprobado'),
('17447770', '32', 'No Aprobado'),
('17447770', '33', 'No Aprobado'),
('17447770', '34', 'No Aprobado'),
('17447770', '35', 'No Aprobado'),
('17447770', '36', 'No Aprobado'),
('17447770', '37', 'No Aprobado'),
('17447770', '38', 'No Aprobado'),
('17447770', '39', 'No Aprobado'),
('17447770', '4', 'Aprobado'),
('17447770', '40', 'No Aprobado'),
('17447770', '41', 'No Aprobado'),
('17447770', '42', 'No Aprobado'),
('17447770', '43', 'No Aprobado'),
('17447770', '44', 'No Aprobado'),
('17447770', '45', 'No Aprobado'),
('17447770', '46', 'No Aprobado'),
('17447770', '47', 'No Aprobado'),
('17447770', '48', 'No Aprobado'),
('17447770', '49', 'No Aprobado'),
('17447770', '5', 'Aprobado'),
('17447770', '50', 'No Aprobado'),
('17447770', '6', 'Aprobado'),
('17447770', '7', 'Aprobado'),
('17447770', '8', 'Aprobado'),
('17447770', '9', 'Aprobado'),
('17810476', '1', 'Aprobado'),
('17810476', '10', 'Aprobado'),
('17810476', '11', 'Aprobado'),
('17810476', '12', 'Aprobado'),
('17810476', '13', 'Aprobado'),
('17810476', '14', 'Aprobado'),
('17810476', '15', 'Aprobado'),
('17810476', '16', 'Aprobado'),
('17810476', '17', 'Aprobado'),
('17810476', '18', 'Aprobado'),
('17810476', '19', 'Aprobado'),
('17810476', '2', 'Aprobado'),
('17810476', '20', 'Aprobado'),
('17810476', '21', 'Aprobado'),
('17810476', '22', 'Aprobado'),
('17810476', '23', 'Aprobado'),
('17810476', '24', 'Aprobado'),
('17810476', '25', 'Aprobado'),
('17810476', '26', 'No Aprobado'),
('17810476', '27', 'No Aprobado'),
('17810476', '28', 'No Aprobado'),
('17810476', '29', 'No Aprobado'),
('17810476', '3', 'Aprobado'),
('17810476', '30', 'No Aprobado'),
('17810476', '31', 'No Aprobado'),
('17810476', '32', 'No Aprobado'),
('17810476', '33', 'No Aprobado'),
('17810476', '34', 'No Aprobado'),
('17810476', '35', 'No Aprobado'),
('17810476', '36', 'No Aprobado'),
('17810476', '37', 'No Aprobado'),
('17810476', '38', 'No Aprobado'),
('17810476', '39', 'No Aprobado'),
('17810476', '4', 'Aprobado'),
('17810476', '40', 'No Aprobado'),
('17810476', '41', 'No Aprobado'),
('17810476', '42', 'No Aprobado'),
('17810476', '43', 'No Aprobado'),
('17810476', '44', 'No Aprobado'),
('17810476', '45', 'No Aprobado'),
('17810476', '46', 'No Aprobado'),
('17810476', '47', 'No Aprobado'),
('17810476', '48', 'No Aprobado'),
('17810476', '49', 'No Aprobado'),
('17810476', '5', 'Aprobado'),
('17810476', '50', 'No Aprobado'),
('17810476', '6', 'Aprobado'),
('17810476', '7', 'Aprobado'),
('17810476', '8', 'Aprobado'),
('17810476', '9', 'Aprobado'),
('17967786', '1', 'Aprobado'),
('17967786', '10', 'Aprobado'),
('17967786', '11', 'Aprobado'),
('17967786', '12', 'Aprobado'),
('17967786', '13', 'Aprobado'),
('17967786', '14', 'Aprobado'),
('17967786', '15', 'Aprobado'),
('17967786', '16', 'Aprobado'),
('17967786', '17', 'Aprobado'),
('17967786', '18', 'Aprobado'),
('17967786', '19', 'Aprobado'),
('17967786', '2', 'Aprobado'),
('17967786', '20', 'Aprobado'),
('17967786', '21', 'Aprobado'),
('17967786', '22', 'Aprobado'),
('17967786', '23', 'Aprobado'),
('17967786', '24', 'Aprobado'),
('17967786', '25', 'Aprobado'),
('17967786', '26', 'Aprobado'),
('17967786', '27', 'Aprobado'),
('17967786', '28', 'Aprobado'),
('17967786', '29', 'Aprobado'),
('17967786', '3', 'Aprobado'),
('17967786', '30', 'Aprobado'),
('17967786', '31', 'Aprobado'),
('17967786', '32', 'Aprobado'),
('17967786', '33', 'Aprobado'),
('17967786', '34', 'Aprobado'),
('17967786', '35', 'Aprobado'),
('17967786', '36', 'Aprobado'),
('17967786', '37', 'Aprobado'),
('17967786', '38', 'Aprobado'),
('17967786', '39', 'Aprobado'),
('17967786', '4', 'Aprobado'),
('17967786', '40', 'Aprobado'),
('17967786', '41', 'Aprobado'),
('17967786', '42', 'Aprobado'),
('17967786', '43', 'Aprobado'),
('17967786', '44', 'Aprobado'),
('17967786', '45', 'Aprobado'),
('17967786', '46', 'No Aprobado'),
('17967786', '47', 'No Aprobado'),
('17967786', '48', 'No Aprobado'),
('17967786', '49', 'No Aprobado'),
('17967786', '5', 'Aprobado'),
('17967786', '50', 'No Aprobado'),
('17967786', '6', 'Aprobado'),
('17967786', '7', 'Aprobado'),
('17967786', '8', 'Aprobado'),
('17967786', '9', 'Aprobado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

CREATE TABLE IF NOT EXISTS `asignatura` (
  `Codigo` varchar(40) NOT NULL,
  `Nombre_ramo` varchar(100) NOT NULL,
  `Semestre` varchar(10) NOT NULL,
  `Año` varchar(20) NOT NULL,
  `Creditos` int(11) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignatura`
--

INSERT INTO `asignatura` (`Codigo`, `Nombre_ramo`, `Semestre`, `Año`, `Creditos`, `Descripcion`) VALUES
('1', 'Fundamentos Contables', 'Primer', 'Primero', 10, '..'),
('10', 'Ofimática', 'Segundo', 'Primero', 0, '..'),
('11', 'Normativa Contable I', 'Primer', 'Segundo', 0, '..'),
('12', 'Economía', 'Primer', 'Segundo', 10, '..'),
('13', 'Desarrollo de Sistemas de Información', 'Primer', 'Segundo', 10, '..'),
('14', 'Estadísticas', 'Primer', 'Segundo', 10, '..'),
('15', 'Inglés II', 'Primer', 'Segundo', 10, '..'),
('16', 'Normativa Contable', 'Segundo', 'Segundo', 10, ''),
('17', 'Micoeconomía', 'Segundo', 'Segundo', 10, '..'),
('18', 'Sistema de Costeo', 'Segundo', 'Segundo', 10, '..'),
('19', 'Métodos Cuantitativos de Gestión', 'Segundo', 'Segundo', 10, '..'),
('2', 'Habilidades Directivas', 'Primer', 'Primero', 0, '..'),
('20', 'Derecho Laboral', 'Segundo', 'Segundo', 10, '..'),
('21', 'Estados Financieros', 'Primer', 'Tercero', 10, 'Describir ramo'),
('22', 'Macroeconomía', 'Primer', 'Tercero', 10, '..'),
('23', 'Contabilidad Gerencial', 'Primer', 'Tercero', 10, '..'),
('24', 'Metodología de la Investigación..', 'Primer', 'Tercero', 10, ''),
('25', 'Introducción a la Fe', 'Primer', 'Tercero', 10, '..'),
('26', 'Practica Temprana', 'Segundo', 'Tercero', 12, 'Describir ramo'),
('27', 'Código Tributario', 'Segundo', 'Tercero', 12, '..'),
('28', 'Administración de Personal', 'Segundo', 'Tercero', 12, 'Describir ramo'),
('29', 'Marketing', 'Segundo', 'Tercero', 12, 'Describir ramo'),
('3', 'Comportamiento Organizacional', 'Primer', 'Primero', 0, '..'),
('30', 'Ética Cristiana', 'Segundo', 'Tercero', 12, 'Describir ramo'),
('31', 'Control Interno', 'Primer', 'Cuarto', 12, '..'),
('32', 'Impuesto al Valor Agregado I', 'Primer', 'Cuarto', 10, '..'),
('33', 'Sistemas Informáticos para las Organizaciones', 'Primer', 'Cuarto', 12, '..'),
('34', 'Cálculo Financiero y Presupuestario', 'Primer', 'Cuarto', 10, 'Describir ramo'),
('35', 'Certificación I', 'Primer', 'Cuarto', 10, 'Describir ramo'),
('36', 'Introducción a la Auditoria', 'VII', 'Quinto', 10, '..'),
('37', 'Impuesto a las Empresas', 'VIII', 'Cuarto', 12, '..'),
('38', 'Creación de Empresas', 'VIII', 'Cuarto', 10, '..'),
('39', 'Gestión Financiera', 'VIII', 'Cuarto', 10, '|'),
('4', 'Matemáticas', 'Primer', 'Primero', 0, '..'),
('40', 'Certificación II', 'VIII', 'Cuarto', 10, '..'),
('41', 'Auditoria a Estados Financieros', 'IX', 'Quinto', 10, '..'),
('42', 'Impuesto a las Personas', 'IX', 'Quinto', 10, ''),
('43', 'Auditoria Informatica', 'IX', 'Quinto', 12, 'Describir ramo'),
('44', 'Estrategias Financieras', 'IX', 'Quinto', 12, 'Describir ramo'),
('45', 'Certificación III', 'IX', 'Quinto', 12, '..'),
('46', 'Auditoria de Gestion', 'X', 'Quinto', 12, '..'),
('47', 'Auditoria Tributaria', 'X', 'Quinto', 10, '..'),
('48', 'Control de Gestión', 'X', 'Quinto', 12, '..'),
('49', 'Formulacion  y Evaluacion de Proyectos', 'X', 'Quinto', 12, '..'),
('5', 'Inglés I', 'Primer', 'Primero', 0, ''),
('50', 'Práctica Final', 'X', 'Quinto', 10, '..'),
('6', 'Práctica Contable', 'Segundo', 'Primero', 0, '..'),
('7', 'Derecho Comercial', 'Segundo', 'Primero', 0, '..'),
('8', 'Administración', 'Segundo', 'Primero', 0, '..'),
('9', 'Métodos Desicionales', 'Segundo', 'Primero', 0, '..');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE IF NOT EXISTS `empresa` (
  `Rut` varchar(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `Contacto` varchar(100) NOT NULL,
  `Supervisor` varchar(100) NOT NULL,
  `contacto_super` varchar(100) NOT NULL,
  PRIMARY KEY (`Rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `Rut` varchar(20) NOT NULL,
  `Nombres` varchar(100) NOT NULL,
  `Apellidos` varchar(100) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Telefono` varchar(15) NOT NULL,
  `Direccion` varchar(200) NOT NULL,
  `Cargo` varchar(20) NOT NULL,
  PRIMARY KEY (`Rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE IF NOT EXISTS `historial` (
  `Id_historial` varchar(20) NOT NULL,
  `Rut_alumno` varchar(20) NOT NULL,
  `Rut_supervisor` varchar(20) NOT NULL,
  `Rut_empresa` varchar(20) NOT NULL,
  `Año_egreso` date NOT NULL,
  `Nota_practica` int(11) NOT NULL,
  PRIMARY KEY (`Id_historial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `rut` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `permiso` varchar(100) NOT NULL,
  `estado` varchar(10) NOT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`rut`, `nombres`, `apellidos`, `email`, `pass`, `permiso`, `estado`) VALUES
('10274579', 'leticia ', 'muñoz', 'leticia.muñoz@ucm.cl', '123', 'Profesor', '0'),
('15149948', 'catherine', 'gonzalez', 'catherine,pgm@gmail.com', '123', 'Secretaria', '0'),
('17447770', 'Maydelin', 'Gonzalez', 'may.strokes@gmail.com', '123', 'Alumno', '0'),
('17810476', 'Belen', 'Vasquez', 'belen.vasquez@gmail.com', '123', 'Alumno', '0'),
('17931406', 'Juan Pablo', 'Cartes', 'jota.cartes19@gmail.com', '123', 'Administrador', '0'),
('17967786', 'Juan', 'Espinosa', 'juanespinosa@gmail.com', '123', 'Alumno', '0'),
('8153133', 'Gabriel', 'Gonzalez', 'hugo@gmail.com', '123', 'Profesor', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
