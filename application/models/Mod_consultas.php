<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_consultas extends CI_Model {

  function __construct(){
    parent::__construct();
  }


  public function VerificarPK($rut) {

      $query = $this->db->query('select *from alumno where Rut="'.$rut.'"');

      if ($query->num_rows()==0) {

          return "noExiste";
      }
      else {

           return "Existe";
      }
     
      

  }

  public function EditAlumno($Alumno,$user,$codigo) {

            $this->db->where('Rut',$codigo);
            $this->db->update('alumno',$Alumno);

            $this->db->where('rut',$codigo);
            $this->db->update('usuarios',$user);
    
  
  }

   public function Edituser($user,$codigo) {

            $this->db->where('rut',$codigo);
            $this->db->update('usuarios',$user);

  
  }

  public function EditAsignatura($data,$codigo) {

            $this->db->where('Codigo',$codigo);
            $this->db->update('asignatura',$data);
          
  
  }

  public function deleteAsignatura ($codigo) {
            
     $query = $this->db->query('delete from asignatura where Codigo="'.$codigo.'"');
         
          
  }

  public function getAsignatura($codigo) {

      $query = $this->db->query('select * from asignatura where Codigo="'.$codigo.'"')->result();
      return $query;

  }

 

   public function MallaxAlumno($rut){

         $query = $this->db->query('select  alumnoasignaturas.Rut_alumno, 
                                            alumnoasignaturas.Codigo_asignatura, 
                                            alumnoasignaturas.Estado, 
                                            asignatura.Nombre_ramo,
                                            asignatura.Codigo
                                            
                              FROM alumnoasignaturas,asignatura 
                              WHERE alumnoasignaturas.Rut_alumno="'.$rut.'"
                                  and asignatura.Codigo= alumnoasignaturas.Codigo_asignatura')->result();
       
          return $query;
    }


     
   public function AddAlumno($dataAlumno) {

      $query = $this->db->query('select *from alumno where Rut="'.$rut.'"');

      if ($query->num_rows()==0) {
         
          $this->db->insert('alumno',$dataAlumno);
          return "noExiste";
      }
      else {

           return "Existe";
      }


        
   }  
    
  public function AddUsuario($dataUsuario){
      $query = $this->db->query('select *from alumno where Rut="'.$rut.'"');

      if ($query->num_rows()==0) {
          $this->db->insert('usuarios',$dataUsuario);
          return "noExiste";
      }
      else {

           return "Existe";
      }


    

  }   
    
   public function AddAsignatura($datos) {

        $this->db->insert('asignatura',$datos);
   }

   public function getUser($rut){

      $query = $this->db->query('select * from usuarios where rut="'.$rut.'"')->result();
      return $query;

   }

   public function getAlumno ($rut) {
     
      $query = $this->db->query('select * from Alumno where Rut="'.$rut.'"')->result();
      return $query;
  }


    public function getAllAsignaturas () {

        $query= $this->db->query('select * from asignatura')->result();
        return $query;

    }

    public function updatePass($data)
    { // 

    $this->db->where('rut', element('rut',$data));
    $this->db->update('usuarios', $data);
  	}
    public function getAlumnos () {

      $query = $this->db->query('select * from alumno')->result();
      return $query;

    }

    public function getUsuarios()
    {
    $query = $this->db->query('select *from usuarios')->result();
    return $query;
    }

    public function AddFormulario($data){
        $this->db->insert('formulario',$data);

    }
    public function EliminarForm($rut,$tipo){

       $this->db->query('delete from formulario where Rut_alumno="'.$rut.'" and Tipo_practica="'.$tipo.'"');
    }
    public function HTemprana ($rut)
    {
    $query = $this->db->query('select * from usuarios where  permiso="Profesor"  or permiso= "Empresa"')->result();

    return $query;

    }


   


      public function getPracticatemprana(){

      $query= $this->db->query('select * from alumno')->result();
      $Alumno="";
      $data = array();
 
      foreach ($query as $row) {
                
                $Alumno =$row->Rut;
                $Nombres = $row->Nombres." ".$row->Apellidos;
         
                   for ($i=1; $i <51 ; $i++) { 
                      
                        $estado= $this->db->query('select * from alumnoasignaturas where Rut_alumno="'.$Alumno.'"and Codigo_asignatura="'.$i.'"')->result();
                        foreach ($estado as $fila ) {
                                $aux= $fila->Estado;
                            
                           //Verificamos que el recorrido hasta el ramo numero 25 esten aprobados
                                if($aux=="Aprobado"){
                            //Una vez que lleguemos al 25 (ramo antes de prepráctica) verificamos
                            //que el atributo del alumno "practica" sea igual a 0, que quiere decir que no ha aprobado
                            //ninguna de las  2 practicas
                                       if($i==25  and $row->Practica==0 ){
                                                                   
                                           array_push($data, array (
                                                        'Rut'=>$row->Rut,
                                                        'Nombres' => $row->Nombres,
                                                        'Apellidos' => $row->Apellidos,
                                                        'Anio' => $row->Año_ingreso,
                                                        'Tipo' => "Pre-Practica",
                                                      )
                                            );
                                                                    
                                       }
                              //Si la prepractica esta aprobada la eliminamos(pop)
                                       if($i==26 and $row->Practica==0 ){
                                          array_pop($data);
                                       }
                              
                              //Ramo anterior a la práctica. Guardamos en el array       
                                        if($i==45 and $row->Practica==2){
                                           array_push($data, array (
                                                        'Rut'=>$row->Rut,
                                                        'Nombres' => $row->Nombres,
                                                        'Apellidos' => $row->Apellidos,
                                                        'Anio' => $row->Año_ingreso,
                                                        'Tipo' => "Practica",
                                                      )
                                            );
                                          
                                       }
                                       if($i==50 and $row->Practica==2){
                                           array_pop($data);

                                       }
                                   }

                                   else {
                                     break 2;
                                   }
                         }
                    }
           
           }

     return $data;
  
}
 //Se procede a dejar al alumno como "en practica".

public function historialTemprana($dataHistorial,$rut,$tipo) {
        
        if ($tipo=="Pre-Practica"){
            $this->db->query('Update alumno Set Practica="1" Where Rut="'.$rut.'"');
        }
        if ($tipo=="Practica"){
           $this->db->query('Update alumno Set Practica="3" Where Rut="'.$rut.'"');

        }
         
        $this->db->insert('historial',$dataHistorial);

    }
  
    
    public function PracticasActuales(){

    $Alumnos = $this->db->query('select * from alumno')->result();
    $data=array();

     foreach ($Alumnos as $row) {

          
           if($row->Practica=="1" || $row->Practica=="3"){
                //Estableciendo practica=1, quiere decir que esta cursando la pre-practica
                if($row->Practica=="1" ){
                    $tipoPractica="Pre-Practica";
                    array_push($data,
                      array (
                      'Rut'=>$row->Rut,
                      'Nombres' => $row->Nombres,
                      'Apellidos' => $row->Apellidos,
                      'Anio' => $row->Año_ingreso,
                      'Tipo'=>$tipoPractica,
                      )
                    );
                }
               //Estableciendo practica=3, quiere decir que esta cursando la practica

                if($row->Practica=="3" ){
                      $tipoPractica="Practica";
                    array_push($data,
                      array (
                      'Rut'=>$row->Rut,
                      'Nombres' => $row->Nombres,
                      'Apellidos' => $row->Apellidos,
                      'Anio' => $row->Año_ingreso,
                      'Tipo'=>$tipoPractica,
                      )
                    );
                }         
           }
      }
      return $data;
    }


  public function DeshabilitarPract($rut,$tipo){

    /********************* numero 1 quiere decir que esta dando la pre-prectica*****************************/
    $this->db->query('delete from formulario where Rut_alumno="'.$rut.'" and Tipo_practica="'.$tipo.'"');
    $this->db->query('delete from historial where Rut_alumno="'.$rut.'" and Tipo_practica="'.$tipo.'"');
    
    if($tipo=="Pre-Practica"){
        $this->db->query('Update alumno Set Practica="0" Where Rut="'.$rut.'"');

    } 

    if ($tipo=="Practica"){
        $this->db->query('Update alumno Set Practica="2" Where Rut="'.$rut.'"');
    }
   
  }

   public function Fin_practica($rut,$tipo){
          /*2= será igual al termino de la primera practica
          Finalizamos el estado en el historial
          Actualizamos el valor del ramo "PRACTICA TEMPRANA" por "Aprobaado"*/

          if($tipo=="Pre-Practica"){
            $this->db->query('Update alumno Set Practica="2" Where Rut="'.$rut.'"');
            $this->db->query('Update historial set estado="Finalizado" where Rut_alumno="'.$rut.'" and Tipo_practica="'.$tipo.'"');
            $this->db->query('Update alumnoasignaturas set estado="Aprobado" where Rut_alumno="'.$rut.'"and Codigo_asignatura="26"');

          }
          if($tipo=="Practica"){
            $this->db->query('Update alumno Set Practica="4" Where Rut="'.$rut.'"');
            $this->db->query('Update historial set estado="Finalizado" where Rut_alumno="'.$rut.'" and Tipo_practica="'.$tipo.'"');
            $this->db->query('Update alumnoasignaturas set estado="Aprobado" where Rut_alumno="'.$rut.'"and Codigo_asignatura="50"');

          }


    }
    public function Responder($rut,$tipo) {

        $query=$this->db->query('select * from  Formulario  where Rut_alumno="'.$rut.'" and Tipo_practica="'.$tipo.'" ')->result();

        return $query;
    }
     public function Info($rut,$tipo){

    $query=$this->db->query('select * from  historial  where Rut_alumno="'.$rut.'" and Tipo_practica="'.$tipo.'" ')->result();
   
    $profe=""; 
    foreach ($query as $row) {
             $profe= $row->Rut_supervisor;
             $empresa=$row->Rut_empresa;
             $query=$this->db->query('select * from usuarios where rut="'.$profe.'" or rut="'.$empresa.'"')->result();
    }

    
    return $query;
    }
 }