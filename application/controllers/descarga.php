<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Descarga extends CI_Controller {
	
  function __construct()
  {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}	


	public function download1()
	{
		$this->load->helper('download');

		  $data = file_get_contents("download/formulario_practica_temprana.pdf"); // Read the file's contents
          $name = 'formulario_practica_temprana.pdf';
          force_download($name, $data);
	}

	public function download2()
	{
		$this->load->helper('download');

		  $data = file_get_contents("download/formulario_practica_final.pdf"); // Read the file's contents
          $name = 'formulario_practica_final.pdf';
          force_download($name, $data);
	}


}