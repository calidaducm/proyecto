<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Con_consultas extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('mod_consultas');
	}

	public function index(){

	}



	public function getAlumnos(){
		if ($this->session->userdata('logged_in'))
		{
			
			$rut['resultado'] =$this->mod_consultas->getAllAlumnos();

			if ($this->session->userdata('privilegio')=='2')
				{
					$this->load->view('menu/header');
					$this->load->view("secretaria/listadealumnos",$rut);
					$this->load->view('menu/foobar');

				}
			if ($this->session->userdata('privilegio')=='1')//
				{
					$this->load->view('menu/header');
					$this->load->view("profe/listadealumnos",$rut);
					$this->load->view('menu/foobar');

				}
		}
			

	}

	public function getAlumnosNoHabilitados(){
		if ($this->session->userdata('logged_in'))
		{
			
			$rut['resultado'] =$this->mod_consultas->getAllAlumnosNoHabilitados();

			if ($this->session->userdata('privilegio')=='2')
				{
					$this->load->view('menu/header');
					$this->load->view("secretaria/listadealumnos",$rut);
					$this->load->view('menu/foobar');

				}
			if ($this->session->userdata('privilegio')=='1')//
				{
					$this->load->view('menu/header');
					$this->load->view("profe/listadealumnos",$rut);
					$this->load->view('menu/foobar');

				}
		}
			

	}
}