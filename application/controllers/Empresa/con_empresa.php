<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Con_empresa extends CI_Controller {

	 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
    
  	 $this->load->model('Empresa_consultas');

    }

	public function index()
	{
		if ($this->session->userdata('logged_in'))
			{
			    
				$this->load->view('menu/headerEmpresa');
				$this->load->view('empresa/empresa');
				$this->load->view('menu/foobar');
		}
		else {redirect('logear');}
	}

	public function infoPersonal(){


				$rut=$this->session->userdata('rut');
			    $info['empresa']=$this->Alumno_consultas->getAlumno($rut);

				$this->load->view('menu/headerAlumno');
				$this->load->view('empresa/editarInfo',$info);
				$this->load->view('menu/foobar');

	}
	public function EditarInfo(){

			$rut=$this->session->userdata('rut');
			$data = array(
								"Nombres"=>$this->input->post('nombres_'),			
								"Apellidos" =>$this->input->post('apellidos_'),
								"Direccion" =>$this->input->post('direccion_'),
								"Email" =>$this->input->post("email_"),
								"Telefono"=>$this->input->post("telefono_"),
								
								);

					$this->Alumno_consultas->ActualizarDatos($data,$rut);

					$rut=$this->session->userdata('rut');
			        $info['alumno']=$this->Alumno_consultas->getAlumno($rut);
     				$this->load->view('menu/headerAlumno');
	    			$this->load->view('alumno/editarInfo',$info);
		    		$this->load->view('menu/foobar');
	}


}