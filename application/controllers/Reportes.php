<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Reportes extends CI_Controller {
 
    public function index()
    {
        ob_end_clean();
        // Se carga el modelo alumno
        $this->load->model('Mod_consultas');
        // Se carga la libreria fpdf
        $this->load->library('Pdf');
 
        // Se obtienen los alumnos de la base de datos
        $query = $this->Mod_consultas->getAlumnos();
 
        // Creacion del PDF
 
        /*
         * Se crea un objeto de la clase Pdf, recuerda que la clase Pdf
         * heredó todos las variables y métodos de fpdf
         */
        $this->pdf = new Pdf();
        // Agregamos una página
        $this->pdf->AddPage();
        // Define el alias para el número de página que se imprimirá en el pie
        $this->pdf->AliasNbPages();
 
        /* Se define el titulo, márgenes izquierdo, derecho y
         * el color de relleno predeterminado
         */
        

        $this->pdf->SetTitle("Lista de actividades");
        $this->pdf->SetLeftMargin(15);
        $this->pdf->SetRightMargin(5);
        $this->pdf->SetFillColor(200,200,200);
 
        // Se define el formato de fuente: Arial, negritas, tamaño 9
        $this->pdf->SetFont('Arial', 'B', 9);
        /*
         * TITULOS DE COLUMNAS
         *
         * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
         */
 
        $this->pdf->Cell(20,7,'Rut','TBL',0,'L','1');
        $this->pdf->Cell(25,7,'Nombres','TB',0,'L','1');
        $this->pdf->Cell(27,7,'Apellidos','TB',0,'L','1');
        $this->pdf->Cell(20,7,'Direccion','TB',0,'L','1');
        $this->pdf->Cell(45,7,'Email','TB',0,'L','1');
        $this->pdf->Cell(30,7,'Telefono','TB',0,'L','1');
        //$this->pdf->Cell(20,7,'Costo','TBR',0,'L','1');
        //$this->pdf->Cell(10,7,'Estado','TBR',0,'L','1');
        $this->pdf->Ln(7 );
        
        
        foreach ($query as $alumnos) {
           
            // Se imprimen los datos de cada alumnos
            $this->pdf->Cell(20,7,$alumnos->Rut,'BL',0,'L',0);
            $this->pdf->Cell(25,7,$alumnos->Nombres,'B',0,'L',0);
            $this->pdf->Cell(27 ,7,$alumnos->Apellidos,'B',0,'L',0);
            $this->pdf->Cell(20,7,$alumnos->Direccion,'B',0,'L',0);
            $this->pdf->Cell(45,7,$alumnos->Email,'B',0,'L',0);
            $this->pdf->Cell(30,7,$alumnos->Telefono,'B',0,'L',0);
           //$this->pdf->Cell(10,7,$alumnos->Estado,'BR',0,'L',0);
            

            //Se agrega un salto de linea
            $this->pdf->Ln(7);
        }
        /*
         * Se manda el pdf al navegador
         *
         * $this->pdf->Output(nombredelarchivo, destino);
         *
         * I = Muestra el pdf en el navegador
         * D = Envia el pdf para descarga
         *
         */
        $this->pdf->Output("Lista de Actividades.pdf", 'I');
    }
}