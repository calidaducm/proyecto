<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cambiar_clave extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('mod_consultas');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in'))
			{
			$info['rut']=$this->session->userdata('rut');
			$this->load->view('menu/header');
			$this->load->view('cambio/pass',$info);
			$this->load->view('menu/foobar');
		}
		else {redirect('logear');}
	}

	public function verificar()
	{
		$pass1 =$this->input->post('pwd');
		$pass2 =$this->input->post('pwd2');
		if($pass1 == $pass2)
		{
				$data = array(
				"rut" =>$this->input->post('rut_'),
				"pass" =>$this->input->post('pwd'));

				$this->mod_consultas->updatePass($data);

				$this->load->view('menu/header');
				$this->load->view('cambio/cambioexitoso');
				$this->load->view('menu/foobar');

		}

		else
		{
			$this->load->view('menu/header');
			$this->load->view('cambio/cambioerror');
			$this->load->view('menu/foobar');
		}



	}
	public function Modificar()
	{
		$pass1 =$this->input->post('pwd');
		$pass2 =$this->input->post('pwd2');
		if($pass1 == $pass2)
		{
				$data = array(
				"rut" =>$this->input->post('rut_'),
				"pass" =>$this->input->post('pwd'));

				$this->mod_consultas->updatePass($data);

				$this->load->view('menu/header');
				$this->load->view('cambio/cambioexitoso');
				$this->load->view('menu/foobar');

		}

		else
		{
			$this->load->view('menu/header');
			$this->load->view('cambio/cambioerror');
			$this->load->view('menu/foobar');
		}



	}

}

