<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Con_profeguia extends CI_Controller {
 function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
    
  	 $this->load->model('Profesor_consultas');
  	 $this->load->model('Mod_consultas');
  }


	public function index()
	{
		if ($this->session->userdata('logged_in'))
			{
			$this->load->view('menu/headerProfesor');
			$this->load->view('profe/tutor');
			$this->load->view('menu/foobar');
			
		}
		else {redirect('logear');}
	}


	public function visualizarAlumnos() { //Metodo para ver el avance en la malla de cada alumno


			$rut['alumnos'] = $this->Profesor_consultas->getAlumnos();
			//$data['ramos']  = $this->Profesor_consultas->getAllAsignaturas();
			$this->load->view('menu/headerProfesor');	
	     	$this->load->view('profe/verMalla',$rut);
			$this->load->view('menu/foobar');

	}


  	public function PracticaTemprana() {
  


  	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Profesor" ){
		
					
			  		 $practicas['alumnos']=$this->Mod_consultas->getPracticatemprana();	


			  		 $this->load->view('menu/headerProfesor');
			  		 $this->load->view("profe/PracticaTemprana",$practicas);
			  		 $this->load->view('menu/foobar');
			  	}
  		}

		else {redirect('logear');}

  		
  	}

  	public function PracticaFinal() {
		  
				  		 $this->load->view('menu/headerProfesor');
	  	if (($this->session->userdata('logged_in'))==TRUE)
			{
				if (($this->session->userdata('privilegio'))=="Profesor" ){
			
						 $practicas['alumnos']=$this->Mod_consultas->getPracticaFinal();	

				  		 $this->load->view('menu/headerProfesor');
				  		 $this->load->view("profe/PracticaFinal",$practicas);
				  		 $this->load->view('menu/foobar');
				  	}
	  		}

			else {redirect('logear');}

  		
  	}




}