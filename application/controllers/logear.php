<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logear extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$this->load->view('menu/header');
	//	$this->load->view('admin/selectores_jquery');
		$this->load->view('login');
		$this->load->view('menu/foobar');
	}

	public function verificar()
	{
		{
			
			$user =$this->input->post('rut_id');
			$passw =$this->input->post('inputPassword');

			$flag = false;
			$this->db->where('rut',$user);
			$this->db->select('rut');
			$this->db->select('pass');
			$this->db->select('permiso');
			$this->db->select('estado');
			$data = $this->db->get('usuarios',1);

			if($data->num_rows()>0)
			{
				if(($data->row()->rut == $user)&&($data->row()->pass == $passw)&&($data->row()->estado == '0'))
				{
					if($data->row()->permiso =='Administrador'){ //permiso 0 = admin
						$newdata = array(
	                   		'username'  =>$data->row()->nombre,
	                   		'logged_in' => TRUE,
	                   		'privilegio'=>$data->row()->permiso,
	                   		'rut'=>$data->row()->rut
               			);

						$this->session->set_userdata($newdata);

						redirect("con_admin");
					}

					if($data->row()->permiso =='Profesor'){ //permiso 1 = profesor guia o jefe carrera
						$newdata = array(
                   		'username'  =>$data->row()->nombre,
                   		'logged_in' => TRUE,
                   		'privilegio'=>$data->row()->permiso,
                   		'rut'=>$data->row()->rut
               			);
						$this->session->set_userdata($newdata);

						redirect("Profesor/con_profeguia");
					}

					if($data->row()->permiso =='Secretaria'){ //permiso 2 = Secretaria
						$newdata = array(
                   		'username'  =>$data->row()->nombre,
                   		'logged_in' => TRUE,
                   		'privilegio'=>$data->row()->permiso,
                   		'rut'=>$data->row()->rut
               			);
						$this->session->set_userdata($newdata);

						redirect("Secretaria/con_secretaria");
					}

					if($data->row()->permiso =='Alumno'){ //permiso 3 = alumno
						$newdata = array(
                   		'username'  =>$data->row()->nombre,
                   		'logged_in' => TRUE,
                   		'privilegio'=>$data->row()->permiso,
                   		'rut'=>$data->row()->rut
               			);
						$this->session->set_userdata($newdata);

						redirect("Alumno/con_alumno");
					}
				}else
					{
						
						redirect('logear');
						
					}
			}		
			else{
					redirect('logear');
				}
		
		}

	}


	public function quienes()
	{
		if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador")
			{
				redirect("con_admin");
			}
			if (($this->session->userdata('privilegio'))=="Profesor")
			{
				redirect("Profesor/con_profeguia");
			}
			if (($this->session->userdata('privilegio'))=="Secretaria")
			{
				redirect("Secretaria/con_secretaria");
			}
			if (($this->session->userdata('privilegio'))=="Alumno")
			{
				redirect("Alumno/con_alumno");
			}




		}
		else {redirect('logear');}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */