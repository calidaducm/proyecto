<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Con_admin extends CI_Controller {

 
  function __construct() {
    //ejecutamos controlador del padre
    parent::__construct();
    
  	 $this->load->model('Mod_consultas');

  }


  public function index()
	{
		if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		
			$this->load->view('menu/header');	
		//	$this->load->view('admin/admin');
			$this->load->view('menu/foobar');
		 }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	
	}



//Una vez finalizada su practica se le cambia el valor a la tabla, en la columna "estado"
public function FinalizarPractica(){

	$rut=$_POST['rut_alumno'];
	$tipo=$_POST['tipo'];
	$this->Mod_consultas->Fin_practica($rut,$tipo);

}



public function ObtenerInfo() {

		$rut=$_POST['rut_alumno'];
		$tipo=$_POST['tipo'];

		$query = $this->Mod_consultas->Info($rut,$tipo);

		$data = array();
		     	foreach ($query as $row) {
				  		array_push($data,
						     		array (
						     		'Nombres'=>$row->nombres,
						     		'Apellidos' => $row->apellidos,	
						     		'Categoria'=>$row->permiso,					     		
				             		)
				    	);
		       	}
		 echo json_encode($data);


}


//En el caso que ocurra un error en haber habilitado a un alumno que no correspondía
//se puede desahabilitar, borrando su historial, y sus preguntas
public function DeshabilitarPractica (){

		$rut=$_POST['rut_alumno'];
		$tipo=$_POST['tipo'];

		$this->Mod_consultas->DeshabilitarPract($rut,$tipo);
		//redirect('admin/Practicantes');
		$aux['practicantes']=$this->Mod_consultas->PracticasActuales();

        $this->load->view('menu/header');
	    $this->load->view("admin/enPractica",$aux);
	    $this->load->view('menu/foobar');


}

//Una vez habilitado un alumno se necesita generar un formulario para que lo evalúe el profesor
//y la empresas, para asi obtener un porcentaje de aprobación

public function GuardarFormulario(){


		for ($i=1; $i <=$_POST['cantidad_preguntas'] ; $i++) { 
			 $dataFormulario= array(
			 						"Id"=>"",
			 				        "Rut_alumno" =>$_POST['rut_alumno'],
			 				        "Tipo_practica"=>$_POST['tipo'],
			 				        "Pregunta"=>$_POST['pregunta'.$i],
			 				        "Respuesta"=>"En proceso",
			 				        );
			 $this->Mod_consultas->AddFormulario($dataFormulario);
		}		
}

//Se responde el formulario creado por el administrador o profesor
public function ResponderF(){

		$rut=$_POST['rut_alumno'];
		$tipo=$_POST['tipo'];
		$query= $this->Mod_consultas->Responder($rut,$tipo);

		$data=array();
		
		foreach ($query as $row) {
			    array_push($data,
						     		array (
						     		'Pregunta'=>$row->Pregunta,
						     		)
				    	);
		}
		
		echo json_encode($data);

}
public function EliminarF(){

		$rut=$_POST['rut_alumno'];
		$tipo=$_POST['tipo'];
		$this->Mod_consultas->EliminarForm($rut,$tipo);


}

//Opcion para editar el formulario

public function VerificarRut($rut){

		$otro = $this->input->post("rut_");
		$data = $this->Mod_consultas->VerificarPK($otro);

		echo json_encode($data);



}

//Guardamos el historial una vez habilitado el alumno
public function GuardarHistorialInicial() {

			$rut = $this->input->post('rut_alumno');
			$tipo = $this->input->post('aux_tipo');
			$dataHistorial = array(
								"Rut_alumno" =>$rut,
								"Tipo_practica"=>$tipo,				
								"Rut_supervisor" =>$this->input->post('profesores_'),
								"Rut_empresa" =>$this->input->post('empresas_'),
								"Nota_practica" =>$this->input->post("0"),
								"Inicio_practica"=>$this->input->post("inicio_practica"),
								"Fin_practica"=>$this->input->post("inicio_practica"),
								"Estado"=>"En proceso",
								);

					 $this->Mod_consultas->historialTemprana($dataHistorial,$rut,$tipo);

					 $practicas['alumnos']=$this->Mod_consultas->getPracticatemprana();	
     		  		 $this->load->view('menu/header');
			  		 $this->load->view("admin/practicantesTemprana",$practicas);
			  		 $this->load->view('menu/foobar');

}

//Obtenemos la informacion de la empresa y profesor que encuentra haciendo la practica
public function getProfesoresEmpresas ($rut) {
		$query= $this->Mod_consultas->HTemprana($rut);

    	$data = array();
		     	foreach ($query as $row) {
				  		array_push($data,
						     		array (
						     		'Rut'=>$row->rut,
						     		'Nombres' => $row->nombres,
						     		'Apellidos' => $row->apellidos,
						     		'Categoria'=>$row->permiso,
				             		)
				    	);
		       	}

		 echo json_encode($data);			         

}

//Obtenemos los alumnos que actualmente estan en practica o prépractica.
public function Practicantes () {

	  
      
      $aux['practicantes']=$this->Mod_consultas->PracticasActuales();

      $this->load->view('menu/header');
	  $this->load->view("admin/enPractica",$aux);
	  $this->load->view('menu/foobar');

}


/***************************HABILITAR PRActIcA TEMPRANA Y PRAcTIcA FINAL*****************************/

  	public function PracticaTemprana() {
  	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria"  ){
		
					
			  		 $practicas['alumnos']=$this->Mod_consultas->getPracticatemprana();	


			  		 $this->load->view('menu/header');
			  		 $this->load->view("admin/practicantesTemprana",$practicas);
			  		 $this->load->view('menu/foobar');
			  	}
  		}

		else {redirect('logear');}

  		
  	}

    public function historial() { //Metodo para ver el avance en la malla de cada alumno

		if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || 
				($this->session->userdata('privilegio'))=="Secretaria"    || 
				($this->session->userdata('privilegio'))=="Profesor" ){


						$this->load->view('menu/header');	
				     	$this->load->view('admin/historialAlumno');
						$this->load->view('menu/foobar');

			}
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	

	}
/***********************************VISUALIZAR MALLA*********************************************/
  	public function achurarMalla() {
  
  	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || 
				($this->session->userdata('privilegio'))=="Secretaria"    || 
				($this->session->userdata('privilegio'))=="Profesor" ){
            
            $Rut = $this->input->post("RutAlumno");
  		   
  		    for ($i=1; $i <= 50; $i++) { 
  										     
			     $Estado= $this->input->post("Estado".$i);
			     //$ramo = $this->input->post("".$i);
				 $this->db->query('update alumnoasignaturas set Estado="'.$Estado.'" where Rut_alumno=
                         		"'.$Rut.'" and Codigo_Asignatura= "'.$i.'"');
				 if($i==26 and $Estado=="Aprobado"){
				 	$this->db->query('update alumno set Practica="2" where rut="'.$Rut.'"');  //Pre-practica aprobada pero no quiere decir que puede hacer la practica, faltan ramos
				 }
				 if($i==50 and $Estado=="Aprobado"){
				 	$this->db->query('update alumno set Practica="4" where rut ="'.$Rut.'"');  //Practica aprobada
				 }
        
  			 }	

  			$rut['alumnos'] = $this->Mod_consultas->getAlumnos();
			$this->load->view('menu/header');	
	     	$this->load->view('admin/visualizarMalla',$rut);
			$this->load->view('menu/foobar');
    
  		}
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	

  	}
  

  	public function MallaAchurada ($rutAlumno) {


	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || 
				($this->session->userdata('privilegio'))=="Secretaria"    || 
				($this->session->userdata('privilegio'))=="Profesor" ){

			  		 $data['resultado']= $this->Mod_consultas->MallaxAlumno($rutAlumno);
			  		 echo json_encode($data);
  		 }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	
  	}

  	public function buscarMallaxAlumno($rut) {

  			if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || 
				($this->session->userdata('privilegio'))=="Secretaria"    || 
				($this->session->userdata('privilegio'))=="Profesor" ){
		
  		
  		$query =$this->Mod_consultas->getRamosAlumnos($rut);//este es el resultado -> algo que muestra en el formulario
     	$data = array();
     	}
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	
  	}


	public function visualizarAlumnos() { //Metodo para ver el avance en la malla de cada alumno

	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		
			$rut['alumnos'] = $this->Mod_consultas->getAlumnos();
			$data['ramos']  = $this->Mod_consultas->getAllAsignaturas();
			$this->load->view('menu/header');	
	     	$this->load->view('admin/visualizarMalla',$rut);
			$this->load->view('menu/foobar');
			}
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	

	}
/***********************************GESTION DE USUARIOS*****************************************/
public function agregarUser(){ 


	 if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
			
				$rut=$_POST['rut'];
		        $nombres=$_POST['nombres'];
		        $email=$_POST['email'];
		        $contra=$_POST['pass'];
		        $estado=$_POST['estado'];
		        $telefono=$_POST['telefono'];
		        $direccion=$_POST['direccion'];
		        $permiso=$_POST['permiso'];

		        $dataUsuario = array(
					"rut" =>$_POST['rut'],
					"nombres" =>$_POST['nombres'],
					"email" =>$_POST['email'],
					"pass" =>$_POST['pass'],
					"permiso"=>$_POST['permiso'],
					"estado"=>$_POST['estado'],
					"telefono"=>$_POST['telefono'],
					"direccion"=>$_POST['direccion'],
     			);

		        if ($permiso=="Alumno"){
		        	$dataUsuario=array("apellidos"=>$_POST['apellidos'],)+$dataUsuario;
		        	
		        	$dataAlumno = array(
						"Rut" =>$_POST['rut'],
						"Nombres" =>$_POST['nombres'],
						"Email" =>$_POST['email'],
						"Estado"=>$_POST['estado'],
						"Telefono"=>$_POST['telefono'],
						"Direccion"=>$_POST['direccion'],
						"Año_ingreso"=>$_POST['anio'],
						"Apellidos"=>$_POST['apellidos'],
					    "Practica"=>"0",
	     			   );
	     				//Asignandole inicialmente al alumno sus ramos no aprobados.
						for ($i=0; $i <50 ; $i++) {
								$row = array(
				                      "Rut_alumno" => $_POST['rut'],
				                      "Codigo_Asignatura" => $i+1,
				                      "Estado" =>"No Aprobado",
				                 );
				                 $this->db->insert('alumnoAsignaturas',$row);
				        }
						

						$mensaje= $this->Mod_consultas->AddAlumno($dataAlumno);
						
					

					

		        }
		        if($permiso=="Empresa"){
	            	$dataUsuario=array("apellidos"=>"",)+$dataUsuario;


		        }
		          if($permiso=="Administrador" || $permiso=="Profesor" || $permiso=="Secretaria"){
	            	$dataUsuario=array("apellidos"=>$_POST['apellidos'],)+$dataUsuario;


		        }

	           

				$mensaje= $this->Mod_consultas->AddUsuario($dataUsuario);
					$this->load->view('menu/header');
					$this->load->view('admin/registroexitoso');
					$this->load->view('menu/foobar');

				

		   }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	
}

public function EditarAlumno() {

	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		
				$codigo = $this->input->post('rut_');
						
						$Alumno = array(
						
						"Nombres" =>$this->input->post('nombres_'),
						"Apellidos" =>$this->input->post('apellidos_'),
						"Año_ingreso" =>$this->input->post('anio_'),
						"Direccion" =>$this->input->post('direccion_'),
						"Email" =>$this->input->post('email_'),
						"Telefono" =>$this->input->post('telefono_'),
						"Estado" =>$this->input->post('estado_'),
					    );

					    $usuario = array(
					    		"Nombres" =>$this->input->post('nombres_'),
								"Apellidos" =>$this->input->post('apellidos_'),
								"Email" =>$this->input->post('email_'),
								"Estado" =>$this->input->post('estado_'),
					         );

						$this->Mod_consultas->EditAlumno($Alumno,$usuario,$codigo);
		            	redirect("con_admin/gestionUser");
		         }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	


}
public function editarUser($rut) {

	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		
				$codigo = $this->input->post('rut_');
						
						$usuario = array(
						
						"Nombres" =>$this->input->post('nombres_'),
						"Apellidos" =>$this->input->post('apellidos_'),
						//"Direccion" =>$this->input->post('direccion_'),
						"Email" =>$this->input->post('email_'),
						"Estado" =>$this->input->post('estado_'),
					    );

					    

						$this->Mod_consultas->Edituser($usuario,$codigo);
		            	redirect("con_admin/gestionUser");
		         }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	


}
public function buscarUser($rut) {

    
			if (($this->session->userdata('logged_in'))==TRUE)
					{
				if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
			

				     $query =$this->Mod_consultas->getUser($rut);//este es el resultado -> algo que muestra en el formulario
				     $data = array();

				     	foreach ($query as $row) {

				     		$data['rut']=$row->rut;
				     		$data['nombres'] = $row->nombres;
				     		$data['apellidos'] = $row->apellidos;
				     		$data['direccion']=$row->direccion;
				     		$data['email'] = $row->email;
				     		$data['telefono'] = $row->telefono;
				     		$data['permiso'] = $row->permiso;
				     		$data['estado'] = $row->estado;


				     	}
				     	echo json_encode($data);

				 }
					 else 	{redirect('Error');}
				}

				 else {redirect('logear');}	

}

public function buscarAlumno($rut) {

    
if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		

			     $query =$this->Mod_consultas->getAlumno($rut);//este es el resultado -> algo que muestra en el formulario
			     $data = array();

			     	foreach ($query as $row) {

			     		$data['rut']=$row->Rut;
			     		$data['nombres'] = $row->Nombres;
			     		$data['apellidos'] = $row->Apellidos;
			     		$data['anio'] = $row->Año_ingreso;
			     		$data['direccion'] = $row->Direccion;
			     		$data['email'] = $row->Email;
			     		$data['telefono'] = $row->Telefono;
			     		$data['estado'] = $row->Estado;


			     	}
			     	echo json_encode($data);

			 }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	

 	}


		public function gestionUser() {
			if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		
		
		    $rut['resultado'] = $this->Mod_consultas->getUsuarios();
    	    $this->load->view('menu/header');	
	     	$this->load->view('admin/GestionUsuarios',$rut);
			$this->load->view('menu/foobar');
			}
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	

	}
	/**************************GESTION DE ASIGNATURAS******************************************/

	public function gestionAsignaturas(){

  	if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador"){
					  $data['query'] = $this->Mod_consultas->getAllAsignaturas();
					  $this->load->view('menu/header');	
					  $this->load->view('admin/Asignaturas',$data);
					  $this->load->view('menu/foobar');
					}
			
		
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	

	}
	  	public function EditarAsignatura ($codigo) {

  		if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		
		  		$query =$this->Mod_consultas->getAsignatura($codigo);//este es el resultado -> algo que muestra en el formulario
		     	$data = array();
		     	foreach ($query as $row) {

				     		$data['Codigo']=$row->Codigo;
				     		$data['Nombre_ramo'] = $row->Nombre_ramo;
				     		$data['Semestre'] = $row->Semestre;
				     		$data['Anio'] = $row->Año;
				     		$data['Creditos'] = $row->Creditos;
				     		$data['Descripcion'] = $row->Descripcion;

		       	}

		     	echo json_encode($data);

		    }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	

  	}

  	public function EliminarAsignatura($codigo) {
      
      if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		
            $this->Mod_consultas->deleteAsignatura($codigo);
            redirect("con_admin/gestionAsignaturas");
      
      }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}	
  }


public function UpdateAsignatura() {

		if (($this->session->userdata('logged_in'))==TRUE)
		{
			if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		

				$codigo = $this->input->post('codigo_');
				$data = array(
				
				"Nombre_ramo" =>$this->input->post('nombre_'),
				"Semestre" =>$this->input->post('semestre_'),
				"Año" =>$this->input->post('anio_'),
				"Creditos" =>$this->input->post('creditos_'),
				"Descripcion" =>$this->input->post('descripcion_'),
				);

				$this->Mod_consultas->EditAsignatura($data,$codigo);
            	redirect("con_admin/gestionAsignaturas");
           }
				 else 	{redirect('Error');}
			}

			 else {redirect('logear');}


}

	public function addAsignatura() {

			if (($this->session->userdata('logged_in'))==TRUE)
				{
					if (($this->session->userdata('privilegio'))=="Administrador" || ($this->session->userdata('privilegio'))=="Secretaria" ){
		     			$a=$this->input->post('codigo_');
						$data = array(
									"Codigo" =>$this->input->post('codigo_'),
									"Nombre_ramo" =>$this->input->post('nombre_'),
									"Semestre" =>$this->input->post('semestre_'),
									"Año" =>$this->input->post('anio_'),
									"Creditos" =>$this->input->post('creditos_'),
									"Descripcion" =>$this->input->post('descripcion_'),
									);
							
							$this->Mod_consultas->AddAsignatura($data);

							$data['query'] = $this->Mod_consultas->getAllAsignaturas();
							$this->load->view('menu/header');	
							$this->load->view('admin/Asignaturas',$data);
							$this->load->view('menu/foobar');

					  }
					 else 	{redirect('Error');}
			   }

			else {redirect('logear');}	
			}




  }