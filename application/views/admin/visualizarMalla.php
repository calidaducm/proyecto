

<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Alumnos</h3> 
                   </div>
                      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="tablaMalla">
                          <thead> 
                                  <tr>
                                      <th>Rut <b class="caret"></b></th>
                                      <th>Nombres</th>
                                      <th>Apellidos</th>
                                      <th>Opción</th>
                                 </tr>
                          </thead> 
                          
                          <tbody> 
                               <?php
                               foreach($alumnos as $row):?>

                                     <tr>
                                         <td><?=$row->Rut?></td>
                                         <td><?=$row->Nombres?></td>
                                         <td><?=$row->Apellidos?></td>
                                         <td>
                                         <!--
                                          <button id="<?=$row->Rut?>" type="button" class="btn btn-info btn-sm" >
                                             <i class="glyphicon glyphicon-eye-open"></i> 
                                                 Mostrar malla
                                          </button>-->
                                          
                                          <a href="<?=$row->Rut?>" type="<?=$row->Nombres.' '.$row->Apellidos?>" class="AchurarMalla">
                                          <button type="button" class="btn btn-info btn-sm" >
                                             <i class="glyphicon glyphicon-edit"></i> 
                                                 Achurar malla
                                          </button>
                                          </a>

                                          </td>  
                                         
                                    </tr> 
                                   
                               <?php endforeach;?> 
                         </tbody>
                   </table>
              </div>           
          </div>
       </div> 
    </div>
</div>
 
  
<div id ="malla" class="container" >
     <div class="row">
           <div class="panel panel-primary">
                  <div  class="panel-heading">
                   <h3 class="panel-title">Malla curricular</h3>
                    <div id="mallaAlumno">
                   </div>  
                   </div>       
          <div class="panel-body">
           <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" >
               <form action="achurarMalla" method="POST">
                    <thead> 
                              <tr>
                                  <th colspan="2">Primer año</th>
                                  <th colspan="2">Segundo año</th>
                                  <th colspan="2">Tercer año</th>
                                  <th colspan="2">Cuarto año</th>
                                  <th colspan="2">Quinto año</th>
                                 
                              </tr>
                      </thead> 
                      
                      <tbody> 
 
                       <?php for ($j=1; $j <= 5; $j++) { ?>      

                          <tr>
                                <?php for ($i=0; $i <=9 ; $i++) { ?>
                                        <td >                     
                                            <a href="<?=$j+5*$i?>">
                                               <div id= "<?=$j+5*$i?>"  class="ramo zoom">
                                                        <div class="" align="center">
                                                            <label>
                                                            <input   style="width:95px; font-size: 8pt"  id= "Codigo<?=$j+5*$i?>" name ="Codigo<?=$j+5*$i?>" disabled> </input><br>
                                                            <input   style="width:95px; font-size: 8pt"  id= "Estado<?=$j+5*$i?>" name ="Estado<?=$j+5*$i?>" readonly="readonly" ></input>
                                                            </label>
                                                       </div> 
                                              </div>
                                             </a>                                    
                                        </td> 
                                <?php }; ?> 
                            </tr>
                        <?php } ?>
                                   
                    </tbody>
               </table>
                    <button type="submit" class="btn btn-info btn-sl " align="center">Finalizar Achurado</button>
                    <input id="btnAchurarMalla" name="RutAlumno" type="hidden"></input>
                 </form>    
             </div>
          </div>
      </div>
  </div>            



<script type="text/javascript">
       $(document).ready(function (){
              $("#malla").hide("fast");
              });
</script>
<script type="text/javascript">
       $(document).ready(function (){
          
           $("a").click(function(e){
            
             

              if ($(this).attr("class")=="AchurarMalla")  {
                  $("#malla").hide("fast");
                  $("#malla").show("fast");
                  var name_alumno = $(this).attr("type");
                  $("#mallaAlumno").empty();
                  $("#mallaAlumno").append("<h4><label >"+name_alumno+"</label></h4>")

                  e.preventDefault();                        //detiene el redirect.
                 
                  var id = $(this).attr("href");   
                  var url = "<?php echo base_url().'index.php/con_admin'?>"+"/MallaAchurada/"+id;
                         // se obtiene el valor del atributo href de la etiqueta "a"
                  //alert(url);  
                   //Le agregamos al campo de texto el valor del rut del alumno que obtenemos en el link
                   $("#btnAchurarMalla").val(id); 
                   $.ajax({
              
                        url: url,
                        type: "POST",
                        dataType : 'JSON',


                       //Obtenemos los valores de los ramos del alumno, si esta aprobado o no.
                        success: function(data)  {
                           
                          console.log(data);
                          var aux="";
                           for (var i = 0 ; i<= 49; i++) {
                                 
                           //Obtengo el código de la asignatura en el array devuelto                                   
                                aux=data.resultado[i].Codigo_asignatura;      
                           //Se establece el nombre del ramo en la celda correspondiente (div del 1 al 50)             
                                $('#Codigo'+aux).val(data.resultado[i].Nombre_ramo);  

                          //Revisamos en el array devuelto si el valor de "Estado " es igual a "NO APROBADO"    
                                if (data.resultado[i].Estado =="No aprobado") {
                                     $('#Estado'+aux).val("No aprobado");
                                     $('#'+aux).css({ 'opacity' : 1});
                                   //  $('#'+aux).css({ 'background-color':'blue'});
                                     $('#'+aux).removeClass("0"); 
                                     $('#'+aux).attr("class","ramo");
 
                                     
                                 }
                                if (data.resultado[i].Estado =="Aprobado") {
                                     $('#Estado'+aux).val("Aprobado");
                                     $('#'+aux).css({ 'opacity' : 0.4 });
                                     $('#'+aux).removeClass("ramo"); 
                                     $('#'+aux).attr("class","0");

                                }
                                else{

                                     $('#Estado'+aux).val(data.resultado[i].Estado);                                  
                                }

                         
                            }

                        },

                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });
                }
             
          });

       });
</script>







<script>
$(document).on('click','.ramo',function(e){
  e.preventDefault();
  e.stopPropagation();
 
            var id = $(this).attr("id");
            $('#Estado'+id).val("Aprobado");
            $(this).css({ 'opacity' : 0.4 });
            $(this).removeClass("ramo"); 
            $(this).attr("class","0");
 
});
</script>

<script>
$(document).on('click','.0',function(e){
  e.preventDefault();
  e.stopPropagation();

            var id = $(this).attr("id");
            $('#Estado'+id).val("No aprobado");
            $(this).css({ 'opacity' : 1 });
            $(this).removeClass("0"); 
            $(this).attr("class","ramo");
 
});
</script>

<script type="text/javascript" charset="utf-8"> 

      $(document).ready(function() {
          $('#tablaMalla').dataTable({
          
            "language": {
              "sProcessing":    "Procesando...",
              "sLengthMenu":    "Mostrar _MENU_ registros",
              "sZeroRecords":   "No se encontraron resultados",
              "sEmptyTable":    "Ningún dato disponible en esta tabla",
              "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":   "",
              "sSearch":        "Buscar:",
              "sUrl":           "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":    "Último",
                  "sNext":    "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
          //  $("#tablaMalla_length").addClass
          });
          });
</script> 
<style type="text/css">
    .zoom{
        /* Aumentamos la anchura y altura durante 2 segundos */
        transition: width 1s, height 1s, transform 1s;
        -moz-transition: width 2s, height 2s, -moz-transform 2s;
        -webkit-transition: width 1s, height 1s, -webkit-transform 1s;
        -o-transition: width 2s, height 2s,-o-transform 2s;
    }
    .zoom:hover{
        /* tranformamos el elemento al pasar el mouse por encima al doble de
           su tamaño con scale(2). */
        transform : scale(2);
        -moz-transform : scale(2);      /* Firefox */
        -webkit-transform : scale(1.3);   /* Chrome - Safari */
        -o-transform : scale(2);        /* Opera */
    }
</style>

 <!--
<script src="http://localhost/auditoria/mallajs/jquery-2.1.3.min.js"></script>
<script src="http://localhost/auditoria/mallajs/custom.js"></script>-->