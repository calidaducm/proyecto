<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                       <h3 class="panel-title">Alumnos cursando su práctica</h3> 
                  </div>

              <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="example"  >
                    <thead> 
                          <tr >
                              <th>Rut</th>                          
                              <th>Nombres</th>
                              <th>Tipo de Práctica</th>
                              <th>Formulario</th>
                              <th>Opciones</th>
                             
                         </tr>
                    </thead> 
                  
                     <tbody> 
                       <?php foreach($practicantes as $row):?>
                         <tr>
                           
                              <td> <?=$row['Rut']?></td>  
                              <td> <?=$row['Nombres']." ".$row['Apellidos']?></td>
                              <td> <?=$row['Tipo']?></td>
                              <td> <a href="<?=$row['Rut']?>" type="<?=$row['Tipo']?>" class="abrirFormulario" data-toggle="tooltip" data-original-title="Generar nuevo formulario" >
                                    <button  type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#Formulario" >
                                            <i class="glyphicon glyphicon-plus"></i>                                     
                                    </button>
                                    </a>
                                    <a href="<?=$row['Rut']?>" type="<?=$row['Tipo']?>" class="ResponderFormulario" >
                                    <button  type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#Responder" >
                                            <i class="glyphicon glyphicon-edit"></i>                                    
                                    </button>
                                    </a>

                                     <a href="<?=$row['Rut']?>" type="<?=$row['Tipo']?>" class="EliminarForm" >
                                    <button  type="button" class="btn btn-danger btn-sm"  >
                                             <i class="glyphicon glyphicon-minus-sign"></i>                               
                                    </button>
                                    </a>
                             </td>
                              <td align="center">
                                    <a href="<?=$row['Rut']?>" type="<?=$row['Tipo']?>" class="Finalizar" >
                                    <button  type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#Progreso" >
                                            <i class="glyphicon glyphicon-edit"></i> 
                                     Finalizar Práctica
                                    </button>
                                    </a>
                                   
                                    <!--Mostrará al profe guia, la empresa, etc.. el historial-->
                                    <a href="<?=$row['Rut']?>" type="<?=$row['Tipo']?>" class="Informacion">
                                    <button   type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#informar">
                                            <i class="glyphicon glyphicon-info-sign"></i> 
                                     Información
                                    </button>
                                    </a>

                                    <a href="<?=$row['Rut']?>" type="<?=$row['Tipo']?>" class="Deshabilitar" method="POST">
                                    <button   type="button" class="btn btn-danger btn-sm" >
                                            <i class="glyphicon glyphicon-info-sign"></i> 
                                     Deshabilitar
                                    </button>
                                    </a>
                              </td>                        
                        </tr> 
                     <?php endforeach;?>                    
                   </tbody>
             </table>    
	    	</div>
  	</div>
</div>

<!--*****************MODAL PARA VER EL PROGRESO DEL PRACTICANTE**************************-->
<div class="modal fade"  id="Formulario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel panel-primary">
                      
                      <div class="panel-heading">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h5 class="panel-title">Creación del formulario de evaluación</h5> 
                      </div>
                      <div class="panel panel-body">
                        <div  class="form-group">
                              <div id="prueba" class="form-group">
                                  <label>Ingrese las preguntas para evaluar al alumno</label> 
                              </div>
                        <form  id="formPreguntas" action ="GuardarFormulario" method="POST"> 
                                <div class="row"> 
                                     <div class="col-md-4">
                                        Eliga el número de preguntas:
                                      </div>
                                      <div class="col-md-4"> 
                                         <select id="n_preguntas" name="n_preguntas" class="form-control n_preguntas">
                                        <?php for ($i=1; $i <11 ; $i++) { ?> 
                                          <option value="<?=$i?>"><?=$i?></option> 
                                      <?php } ?>
                                      </select>  
                                       </div> 
                                       <div class="col-md-4">
                                             <button id="nuevaPregunta" type="button" class="btn btn-primary btn-sm" >
                                                <i class="glyphicon glyphicon-plus"></i> Agregar preguntas
                                              </button>
                                       </div>
                                  </div>   
                                </div> 
                                 
                                <div id="preguntas" >
                                </div>       
                                <input id="rut_alumno" name="rut_alumno" hidden></input>
                                <input id="tipo_" name="tipo_" hidden></input>

                          <button id="formG" data-dismiss="modal" type="button" class="btn btn-primary" >Guardar preguntas</button>

                      </form> 
                  </div>           
             </div>
        </div>
    </div>
  </div>
</div>
</div>

<!--*****************MODAL PARA VER EL PROGRESO DEL PRACTICANTE**************************-->
<div class="modal fade"  id="informar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel panel-primary">
                      
                      <div class="panel-heading">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h5 class="panel-title">Información sobre la práctica</h5> 
                      </div>
                      <div class="panel panel-body">
                        <div  class="form-group">
                              
                        <form  id="info" action ="GuardarFormulario" method="POST"> 
                                <div class="row"> 
                                     <div class="col-md-4">
                                        Profesor : 
                                      </div>
                                      <div class="col-md-4"> 
                                      <input id="profe_info" class="form-control" type="text" readonly="readonly"></input>
                                      </div> 
                                       <div class="col-md-4">
                                            
                                       </div>
                                  </div>   
                                </div> 
                                <div class="row"> 
                                     <div class="col-md-4">
                                      Empresa : 
                                      </div>
                                      <div class="col-md-4"> 
                                      <input id="empresa_info" class="form-control" type="text" readonly="readonly"></input>
                                      </div> 
                                       <div class="col-md-4">
                                            
                                       </div>
                                  </div>   
                       </form> 
                  </div>           
             </div>
        </div>
    </div>
  </div>
</div>
</div>

<!--**********************************MODAL PARA VER EL PROGRESO DEL PRACTICANTE**************************-->
<div class="modal fade"  id="Responder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel panel-primary">
                      
                      <div class="panel-heading">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h5 class="panel-title">Respuesta sobre la práctica</h5> 
                      </div>
                      <div class="panel panel-body">
                        <div  class="form-group">
                              
                        <form  id="" action ="GuardarFormulario" method="POST"> 
                          <div id="resp"></div>

                       <button id="RespuestasFormulario" data-dismiss="modal" type="button" class="btn btn-primary" >Guardar calificación</button>
                      </form> 
                  </div>           
                 </div>
            </div>
        </div>
      </div>
   </div>
</div>

<!--**********************************MODAL PARA MODIFIcAR EL FORMULARIO**************************-->
<div class="modal fade"  id="Editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel panel-primary">
                      
                      <div class="panel-heading">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h5 class="panel-title">Editar formulario</h5> 
                      </div>
                      <div class="panel panel-body">
                        <div  class="form-group">
                              
                        <form  id="" action ="" method="POST"> 
                          <div id="editF"></div>

                       <button id="btnEditarForm" data-dismiss="modal" type="button" role="" data="" class="btn btn-primary" >Guardar cambios</button>
                      </form> 
                  </div>           
                 </div>
            </div>
        </div>
      </div>
   </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $(".abrirFormulario").tooltip({
      placement : 'bottom',
    
    });

  
    $(".tip-right").tooltip({placement : 'right'});
    $(".tip-bottom").tooltip({placement : 'bottom'});
    $(".tip-left").tooltip({ placement : 'left'});
});
</script>
<script type="text/javascript">
       $(document).ready(function (){
           $(".Finalizar").click(function(e){
                e.preventDefault();
                var url = "<?php echo base_url().'index.php/con_admin'?>"+"/FinalizarPractica";
                var data = $(this).serializeArray();

               
                var rut = $(this).attr("href");
                var tipo= $(this).attr("type");
                console.log(rut+tipo);
                data.push({name:'rut_alumno',value:rut});
                data.push({name:'tipo',value:tipo});

                              
                $.ajax({                  
                            url: url,
                            type: "POST",  
                           // dataType: "JSON",                         
                            data: data,


                            success: function(data)  {
                                console.log(data);
                             //location.href="http://localhost/auditoria/index.php/con_admin/Practicantes"; 
                             },
                            error: function(result) {
                           
                            console.log("Error: " + result.statusText);
                            }
                           
                });

            });
     }); 
 </script> 


<script type="text/javascript">
       $(document).ready(function (){
           $(".Informacion").click(function(e){
                e.preventDefault();
                var url = "<?php echo base_url().'index.php/con_admin'?>"+"/ObtenerInfo";
                var data = $(this).serializeArray();

               
                var rut = $(this).attr("href");
                var tipo= $(this).attr("type");
                console.log(rut+tipo);
                data.push({name:'rut_alumno',value:rut});
                data.push({name:'tipo',value:tipo});

                              
                $.ajax({                  
                            url: url,
                            type: "POST",  
                            dataType: "JSON",                         
                            data: data,


                            success: function(data)  {
                                console.log(data[0]);
                                if (data[0].Categoria=="Profesor"){
                                   $("#profe_info").val(data[0].Nombres+" "+data[0].Apellidos);
                                   $("#empresa_info").val(data[1].Nombres+" "+data[1].Apellidos);
                                }
                                else {
                                   $("#empresa_info").val(data[0].Nombres+" "+data[0].Apellidos);
                                   $("#profe_info").val(data[1].Nombres+" "+data[1].Apellidos);
                                }


                             //location.href="http://localhost/auditoria/index.php/con_admin/Practicantes"; 
                             },
                            error: function(result) {
                           
                            console.log("Error: " + result.statusText);
                            }
                           
                });
              });
     }); 
 </script> 








<script type="text/javascript">
       $(document).ready(function (){
           $(".Deshabilitar").click(function(e){
                e.preventDefault();
                var url = "<?php echo base_url().'index.php/con_admin'?>"+"/DeshabilitarPractica";
                var data = $(this).serializeArray();

               
                var rut = $(this).attr("href");
                var tipo= $(this).attr("type");
                console.log(rut+tipo);
                data.push({name:'rut_alumno',value:rut});
                data.push({name:'tipo',value:tipo});

                              
                $.ajax({                  
                            url: url,
                            type: "POST",                           
                            data: data,

                            success: function(data)  {
                             location.href="http://localhost/auditoria/index.php/con_admin/Practicantes"; 
                             },
                            error: function(result) {
                           
                            console.log("Error: " + result.statusText);
                            }
                           
                });
              });
     }); 
</script>

<script type="text/javascript">
       $(document).ready(function (){
           $(".abrirFormulario").click(function(e){
                e.preventDefault();
                var rut = $(this).attr("href");
                var tipo= $(this).attr("type");
                console.log(rut);
                $("#rut_alumno").attr("value",rut);
                $("#tipo_").attr("value",tipo);

              

          });
     });      
</script> 

<script type="text/javascript">
       $(document).ready(function (){
           $(".EliminarForm").click(function(e){
                e.preventDefault();
                var data = $(this).serializeArray();
                var answer = confirm("¿Seguro que desea eliminar el formulario?","si");
            if (answer){
                   
                var url = "<?php echo base_url().'index.php/con_admin'?>"+"/EliminarF";
                var rut = $(this).attr("href");
                var tipo= $(this).attr("type");
                data.push({name:'rut_alumno',value:rut});
                data.push({name:'tipo',value:tipo});
             
                $.ajax({                  
                            url: url,
                            type: "POST",  
                            data: data,

                            success: function()  {
                            
                            console.log("Todo bien");
                            
                             },
                            error: function(result) {
                           
                            console.log("Error: " + result.statusText);
                            }
                           
                });
             }
               else{
                    e.preventDefault();  
                      alert("Bye bye!")
                   
                  }   
              

          });
     });      
</script> 

<!--Botón editar formulario-->
<script type="text/javascript">
       $(document).ready(function (){
           $("#PRUEBAbtnEditarForm").click(function(e){
              e.preventDefault();
              var rut = $(this).attr("role");
              var tipo= $(this).attr("type");
              var preguntas =$(this).attr("preguntas");
              data.push({name:'rut',value:rut});
              data.push({name:'tipo',value:tipo});
              data.push({name:'n_preguntas',value:preguntas});
              var pregunta="";
              console.log(rut + tipo+ preguntas);
              for (var i = 1 ; i <=preguntas; i++) {
                    pregunta= $(this).attr("Preguntaedit"+i);
                    data.push({name:'Pregunta'+i,value:pregunta});
              };
              $.ajax({                  
                            url: url,
                            type: "POST",  
                            data: data,

                            success: function(data)  {
                            
                            console.log("Todo bien");
                            
                             },
                            error: function(result) {
                           
                            console.log("Error: " + result.statusText);
                            }
                           
                });

       });
     });   
</script> 

<script type="text/javascript">
       $(document).ready(function (){
           $(".EditarFormulario").click(function(e){
                e.preventDefault();
              
                var url = "<?php echo base_url().'index.php/con_admin'?>"+"/ResponderF";
                var data = $(this).serializeArray();

               
                var rut = $(this).attr("href");
                var tipo= $(this).attr("type");
                console.log(rut+tipo);
                data.push({name:'rut_alumno',value:rut});
                data.push({name:'tipo',value:tipo});
                $("#btnEditarForm").attr("type",tipo);
                $("#btnEditarForm").attr("role",rut);



                              
                $.ajax({                  
                            url: url,
                            type: "POST",  
                            dataType:"JSON",                         
                            data: data,

                            success: function(data)  {
                                console.log(data);
                                n=data.length;
                                $("#btnEditarForm").attr("preguntas",n);
                                if(n>0){
                                  console.log("cantida de preguntas: "+n);
                                  
                                                               
                                  var textos="";
                                  for (var i =0; i<n;i++) {
                                       textos = textos +
                                       '<p><div class="row"><div class="col-md-6"><label> Pregunta '+(i+1)+':</label></div><div class="col-md-6"><input id="Preguntaedit"'+(i+1)+'" type="text" class="form-control" value="'+data[i].Pregunta+'"></input></div></div></p>';

                                  }; 
                                  $("#editF").empty();
                                  $("#editF")
     
                                  .append
                                   (       
                                     $(''+textos)
                                   );
                               }
                               else
                               {
                                alert("No se han generado preguntas");

                               }
               
                             },
                            error: function(result) {
                           
                            console.log("Error: " + result.statusText);
                            }
                           
                });
                             

          });
     });      
</script> 


<script type="text/javascript">
       $(document).ready(function (){
           $(".ResponderFormulario").click(function(e){
                e.preventDefault();
              
                var url = "<?php echo base_url().'index.php/con_admin'?>"+"/ResponderF";
                var data = $(this).serializeArray();

               
                var rut = $(this).attr("href");
                var tipo= $(this).attr("type");
                console.log(rut+tipo);
                data.push({name:'rut_alumno',value:rut});
                data.push({name:'tipo',value:tipo});

                              
                $.ajax({                  
                            url: url,
                            type: "POST",  
                            dataType:"JSON",                         
                            data: data,

                            success: function(data)  {
                                console.log(data);
                                n=data.length;
                                if(n>0){
                                console.log("cantida de preguntas: "+n);
                                
                                var opciones='';
                                for (var i = 1; i < 8; i++) {
                                    opciones= opciones+'<option value="'+i+'">'+i+'</option> ';
                                };
                                var porcentajes= '<select id="porcentaje" name="porcentaje" class="form-control">'+opciones+'</select>';
                                
                                
                                var textos="";
                                for (var i =0; i<n;i++) {
                                     textos = textos +
                                     '<p><div class="row"><div class="col-md-6"><label>'+data[i].Pregunta+':</label></div><div class="col-md-6">'+porcentajes+'</div></div></p>';

                                }; 
                                $("#resp").empty();
                                $("#resp")
   
                                .append
                                 (       
                                   $(''+textos)
                                 );
                               }
                               else
                               {
                                alert("No se han generado preguntas");

                               }
               
                             },
                            error: function(result) {
                           
                            console.log("Error: " + result.statusText);
                            }
                           
                });
                             

          });
     });      
</script> 

<script type="text/javascript">
       $(document).ready(function (){
           $("#formG").click(function(e){
              
                var url = "<?php echo base_url().'index.php/con_admin'?>"+"/GuardarFormulario/";
                var data = $(this).serializeArray();

                var n_preguntas= $("#n_preguntas").val();
                var rut = $("#rut_alumno").val();
                var tipo= $("#tipo_").val();

                data.push({name:'cantidad_preguntas',value:n_preguntas});
                data.push({name:'rut_alumno',value:rut});
                data.push({name:'tipo',value:tipo});

                for (var i = 1; i <=n_preguntas; i++) {
                      var pregunta= $("#pregunta"+i).val();
                      data.push({name:'pregunta'+i,value:pregunta});
                };

                
                $.ajax({                  
                            url: url,
                            type: "POST",
                            dataType: "JSON",
                            data: data,

                            error: function(result) {
                            console.log("Error: " + result.statusText);
                            }
                           
                });
              });
     }); 
 </script> 

<script type="text/javascript" charset="utf-8"> 

      $(document).ready(function() {
          $('#example').dataTable();

          });
</script> 

<script type="text/javascript">
    
   $("#nuevaPregunta").on('click',funcNuevoDocumento);


function funcNuevoDocumento() {



      var n = $(".n_preguntas").val();
      console.log("cantida de preguntas: "+n);
      var textos="";
      for (var i =1; i<=n;i++) {
           textos = textos +
           '<p><div class="row"><div class="col-md-3"><label>  Pregunta '+i+':</label></div><div class="col-md-9"><input id="pregunta'+i+'" name= "pregunta'+i+'" type="text" class="form-control" placeholder="Ingrese nueva pregunta"></input></div></div></p>';

      }; 


    $("#preguntas").empty();
    $("#preguntas")
   
    .append
    (
        
                   $(''+textos)
     

    );
}
</script>