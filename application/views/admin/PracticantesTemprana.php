

<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Alumnos habilitados</h3> 
                   </div>

 <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="example"  >
                  <thead> 
                     <tr>
                              <th> Rut </th>
                              <th align="center">Nombres</th> 
                              <th align="center">Apellidos</th> 
                              <th> Año de Ingreso </th>    
                              <th>Tipo de práctica</th>                     
                              <th align="center">Opciones</th>
                             
                     </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($alumnos as $row):?>
                             <tr>
                                                                
                                  <td align="center"> <?=$row['Rut']?> </td>  
                                  <td align="center"> <?=$row['Nombres']?> </td>
                                  <td align="center"> <?=$row['Apellidos']?> </td>     
                                  <td align="center"> <?=$row['Anio']?> </td>   
                                  <td align="center"> <?=$row['Tipo']?> </td>                         
                                  <td align="center">

                                 <!-- Button trigger modal href="buscarUsuarioId/<?=$row->rut?>"-->
                                    
                                   
                                    <a href="<?=$row['Rut']?>" type="<?=$row['Tipo']?>" class="Habilitar">
                                    <button   type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#Habilitar">
                                        <i class="glyphicon glyphicon-edit"></i> 
                                        <label>   Habilitar para <?=$row['Tipo'] ?></label>
                                    </button>
                                    </a>

                                  </td>
                             </tr> 
                        <?php endforeach;?>                    
                 </tbody>
             </table>    
	     	</div>
	  </div>
</div>


<div class="modal fade"  id="Habilitar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel panel-primary">
                      
                      <div class="panel-heading">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h5 class="panel-title">Habilitar alumno</h5> 
                      </div>
                      <div class="panel panel-body">
                        <div class="form-group">
                              <div class="form-group">
                                  <label>Selecione al profesor guía y a la empresa</label> 
                            </div>
                            <form  id="habilitar" action =" GuardarHistorialInicial" method="POST"> 
                             
                                  <input id="aux_tipo" name="aux_tipo" type="text" hidden readonly="readonly">
                                </input>
                                  <div class="row"> 
                                     <div class="col-md-5">
                                        Seleccione profesor guía:
                                      </div>
                                      <div class="col-md-7"> 
                                        <select id = "profesores_" name="profesores_" class="form-control"> 
                                     

                                      </select>  
                                       </div> 
                                  </div> 
                                  <br>
                                  <div class="row"> 
                                     <div class="col-md-5">
                                        Seleccione Empresa:
                                      </div>
                                      <div class="col-md-7"> 
                                         <select id="empresas_" name="empresas_" class="form-control"> 
                                          
                                      </select>  
                                       </div> 
                                  </div> 
                                  <br>
                                  <div class="row">
                                      <div class="col-md-5">
                                        Fecha de inicio de práctica:
                                      </div>
                                       <div class="col-md-7">
                                        <input id ="inicio_practica" name="inicio_practica" type="date" class="form-control"></input>
                                        <input id ="rut_alumno" name="rut_alumno" type="text" hidden></input>
                                      </div>

                                  </div>


                             <button type="submit" class="btn btn-primary" >Guardar cambios</button>
                          </form> 
                    </div>           
                 </div>
             </div>
          </div>
      </div>
    </div>
</div>
<script type="text/javascript">
       $(document).ready(function (){
           $(".Habilitar").click(function(e){
              
                     e.preventDefault();                           //Evita que el evento se genere (redireccionar)
                     var url = "<?php echo base_url().'index.php/con_admin'?>"+"/getProfesoresEmpresas/";
                     var link = $(this).attr("href");             // se obtiene el valor del atributo href de la etiqueta "a"
                     $("#aux_tipo").val( $(this).attr("type") );
                     $("#rut_alumno").val(""+link);
                     
                     $.ajax({                  
                            url: url+link,
                            type: "POST",
                            dataType: "JSON",
                           
                            success: function(data)  {
                            console.log(data);
                            //Limpiamos los select de profesores y empresas 
                            $('#profesores_').empty();
                            $('#empresas_').empty();
                            //Recorremos los vectores, generamos y agregamos los option
                            //con los datos de los profesores y empresas respectivamente
                            for (i = 0; i < data.length; i++) { 
                                   

                                   if (data[i].Categoria=="Profesor"){

                                        $('#profesores_').append('<option value='+data[i].Rut+'>'+data[i].Nombres+' '+data[i].Apellidos+'</option>');
                                        
                                   }
                                   if(data[i].Categoria=="Empresa"){
                                         $('#empresas_').append('<option value='+data[i].Rut+'>'+data[i].Nombres+' '+data[i].Apellidos+'</option>');

                                   }
                            }
                

                            },
                            error: function(result) {
                            console.log("Error: " + result.statusText);
                            }
                      });
               });
    });       
</script>








<script type="text/javascript" charset="utf-8"> 

      $(document).ready(function() {
          $('#example').dataTable();

          });
</script> 