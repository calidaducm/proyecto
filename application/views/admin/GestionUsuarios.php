
<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Alumnos</h3> 
                   </div>
                   
              <br>
           <div class="panel panel-body"> 
             <form>
                                      
                      <div class="btn-group btn-group-justified">
                       
                  
                        <a href="Usuario" class="Usuario" >
                        <button  type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ModalAgregarUsuario">
                            <i class="glyphicon glyphicon-plus"></i> Agregar Usuario
                          </button> 
                        </a>
                       
                                   

                    </div>
            
                 <br>
            </form>
            
              <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="example"  >
                  <thead> 
                    <tr>
                              <th>Rut</th>
                              <th>Nombres</th>
                              <th>Apellidos</th>
                              <th>Email</th>
                              
                              <th>Categoría</th>
                              <th>Opciones</th>
                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($resultado as $row):?>
                             <tr>
                                                                
                                 <td> <?=$row->rut?> </td> 
                                 <td> <?=$row->nombres?> </td>
                                 <td> <?=$row->apellidos?> </td>
                                
                                 <td> <?=$row->email?> </td>
                                 <td> <?=$row->permiso?> </td>
                              
                                  <td class="col-lg-4">

                                 <!-- Button trigger modal href="buscarUsuarioId/<?=$row->rut?>"-->
                                    <a href="<?=$row->rut?>" class="<?=$row->permiso?>">
                                      <button  type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ModalEditar">
                                              <i class="glyphicon glyphicon-edit"></i> 
                                        Editar
                                      </button>
                                    </a>

                                    <a href="<?=$row->rut?>" class="Eliminar">
                                    <button   type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ModalEditar">
                                      <i class="glyphicon glyphicon-minus-sign"></i> 
                                     Deshabilitar
                                    </button>
                                    </a>
                                   
                                  </td>
                             </tr> 
                        <?php endforeach;?>                    
                  </tbody>
             </table>    
         </div>
      </div>
   </div>
</div>
</div>

 <!--********************************* MODAL EDITAR ALUMNO ***************************************-->

<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Editar Alumno</h4>
          </div>
          <div class="modal-body">

               <form  id="formEditar" action ="EditarAlumno" method="POST"> 
                      <div>
                           <div align="center" > 
                            Rut:
                            <input id="Arut_edit" type="text"  class="form-control" name="rut_" /> 

                            Nombres :
                            <input id="Anombres_edit" type="Text" name="nombres_" class="form-control" required />
                            <div id="div_apellidos">
                            Apellidos:
                            <input id="Aapellidos_edit" type="Text" name="apellidos_" class="form-control" required />
                            </div>
                            <div id="div_anio">
                            Año Ingreso:
                            <input id="Aanio_edit" type="Text" name="anio_" class="form-control" required />
                            </div>
                            Direccion:
                            <input id="Adireccion_edit" type="Text" name="direccion_" class="form-control" required />
                            Email:
                            <input id="Aemail_edit" type="Text" name="email_" class="form-control" required />
                            Teléfono:
                            <input id="Atelefono_edit" type="Text" name="telefono_" class="form-control" required />
                            <br/>
                            <select name="Aestado_edit" class="form-control"> 
                                <option value="0">Habilitado</option> 
                                <option value="1">Deshabilitado</option>  
                            </select> 

                            <br />
                            <br />
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                      </div>
                        </div>
                  </div>
              </form>
          </div>
      </div>
    </div>
</div>




 <!--********************************* MODAL AGREGAR ALUMNO ***************************************-->
<div class="modal fade"  id="ModalAgregarUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel panel-primary">
                      
                      <div class="panel-heading">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h5 class="panel-title">Agregar Usuario</h5> 
                      </div>
                      <div class="panel panel-body">
                        <div  class="form-group">
                           <div class="row"> 
                             <div class="col-md-3">
                               <div id="prueba" class="form-group">
                                  <label>Eliga usuario</label> 
                              </div>
                             </div>
                             <div class="col-md-6">
                               <select id="tipo_usuario" name="tipo_usuario" class="form-control n_preguntas">
                                       <option value="Alumno">Alumno</option> 
                                       <option value="Profesor">Profesor</option>
                                       <option value="Secretaria">Secretaria</option>
                                       <option value="Administrador">Administrador</option>
                                       <option value="Empresa">Empresa</option>
                               </select> 
                            </div>
                            <div class="col-md-3">
                              <button id="generar_usuario" type="button" class="btn btn-primary">Aceptar</button>
                            </div>
                        </div>

               <form id="FormIngresarUser" name="FormIngresarUser" action =""  > 

                 
                      <div class="row"> 
                           <div class="col-md-6">
                             Rut
                             <input id ="rut_" name="rut_" type="text"  class="form-control" placeholder="Ingrese Rut" />  
                            </div>  
                           <div class="col-md-6">                                                             
                            Contraseña
                            <input id= "pass_" name="pass_" type="text"  class="form-control" placeholder="Ingrese Contraseña" />
                            </div>
                      </div>
                        <div id="apellidos_div" class="row"> 
                          
                        <div  class="col-md-6">
                            Nombres
                            <input id="nombres_" name= "nombres_" type="Text"  class="form-control" placeholder="Ingrese nombres" required />
                            </div>
                              <div class="col-md-6">
                               Apellidos  
                            <input id ="apellidos_" name= "apellidos_" type="Text"  class="form-control" required />
                        </div>
                      </div>

                      <div id="ingreso_anio" class="row"> 
                           <div class="col-md-12">  
                            Direccion
                            <input id="direccion_" name ="direccion_" type="Text" class="form-control"  />
                            </div>
                      </div>

                       <div class="row"> 
                          <div class="col-md-6">
                            Email
                            <input  id= "email_" name ="email_" type="Text" class="form-control"  />
                           </div>
                           <div class="col-md-6">
                              Telefono
                              <input id="telefono_" name ="telefono_" type="Text" class="form-control"  />
                          </div>
                        </div>
                      <p>
                      Estado
                      <select id="estado_" name="estado_" class="form-control"> 
                      <option value="0">Habilitado</option> 
                      <option value="1">Deshabilitado</option>  
                      </select> 
                    </p>
                      <br />
                     
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button id="btnagregarUsuario" type="button" data-dismiss="modal" class="btn btn-primary">Guardar cambios</button>
                            
                </form>
               
             </div>
          </div>
        </div>
      </div>
   </div>
</div>






<!--*********************************JAVASCRIPT PARA GESTIONAR MANTENEDORES**********************************-->
<script type="text/javascript">
       $(document).ready(function (){
           $("a").click(function(e){
                  
              var clase= $(this).attr("class");

              if ($(this).attr("class")=="Alumno")  {
                  e.preventDefault();                   //Evita que el evento se genere (redireccionar)
                  var url = "<?php echo base_url().'index.php/con_admin'?>"+"/buscarAlumno/";
                  var link = $(this).attr("href");          // se obtiene el valor del atributo href de la etiqueta "a"
                         
                 //location.href(text);                      //Redirige a un link
                  $.ajax({
              
                        url: url+link,
                        type: "POST",
                        dataType : 'JSON',

                        success: function(data)  {
                           console.log(data);
                               $('#Arut_edit').val(data.rut);
                               $('#Anombres_edit').val(data.nombres);
                               $('#Aapellidos_edit').val(data.apellidos);
                               $('#Aanio_edit').val(data.anio);
                               $('#Adireccion_edit').val(data.direccion);
                               $('#Aemail_edit').val(data.email);
                               $('#Atelefono_edit').val(data.telefono);

                               $('#Aestado_edit').val(data.estado);
                          // $('#')
                        },
                        error: function(result) {
                        console.log("Error" + result);
                        }
                  });
               }

              if ((clase=="Secretaria") || (clase=="Profesor") || (clase=="Administrador") || (clase=="Empresa"))
              {

                  e.preventDefault();                         //Evita que el evento se genere (redireccionar)
                  var url = "<?php echo base_url().'index.php/con_admin'?>"+"/buscarUser/";
                  var link = $(this).attr("href");          // se obtiene el valor del atributo href de la etiqueta "a"
                         
                 //location.href(text);                      //Redirige a un link
                  $.ajax({
              
                        url: url+link,
                        type: "POST",
                        dataType : 'JSON',

                        success: function(data)  {
                              console.log(data);
                              
                               if(clase=="Empresa"){

                                 $('#Arut_edit').val(data.rut);
                                 $('#Anombres_edit').val(data.nombres);
                                 //$('#Aapellidos_edit').val(data.apellidos);
                                 $('#Adireccion_edit').val(data.direccion);
                                 $('#Aemail_edit').val(data.email);
                                 $('#Atelefono_edit').val(data.telefono);
                                 $('#Aestado_edit').val(data.estado);
                             //    $("#anoingreso_").attr("type","hidden");
                                 $("#div_anio").hide("fast");
                                 $("#div_apellidos").hide("fast");
                                 $("#formEditar").attr("action","EditarUser/"+link);
                                } 
                                else{
                                 $('#Arut_edit').val(data.rut);
                                 $('#Anombres_edit').val(data.nombres);
                                 $('#Aapellidos_edit').val(data.apellidos);
                                 $('#Adireccion_edit').val(data.direccion);
                                 $('#Aemail_edit').val(data.email);
                                 $('#Atelefono_edit').val(data.telefono);
                                 $('#Aestado_edit').val(data.estado);
                             //    $("#anoingreso_").attr("type","hidden");
                                 $("#div_anio").hide("fast");
                                 //$("#div_apellidos").hide("fast");
                                 $("#formEditar").attr("action","EditarUser/"+link);

                                }      

                          // $('#')
                        },
                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });

              }      


            });
       });
</script>

<script type="text/javascript">
      $(".Usuario").click(function(e){
                  
            e.preventDefault();            
       });
</script>

<!--Scrip para generar el formulario para cada usuario-->
<script type="text/javascript">
       $(document).ready(function (){
           $("#generar_usuario").click(function(e){

              e.preventDefault();
              var user = $("#tipo_usuario").val();


              if (user=="Alumno"){
                
                 $("#ingreso_anio").empty();
                 var textos='<div class="col-md-6">Año ingreso<input id="anio_" name= "anio_" type="Text"  class="form-control" placeholder="Ingrese año de ingreso" required /></div><div class="col-md-6">Dirección <input id ="direccion_" name= "direccion_" type="Text"  class="form-control" required /></div>';
                 $("#ingreso_anio").append( $(''+textos) );
              
            
              }
              if(user=="Empresa"){
                $("#apellidos_div").empty();
                 var textos='<div class="col-md-12">Nombre<input id="nombres_" name= "nombres_" type="Text"  class="form-control" placeholder="Ingrese nombre de la empresa" required /></div>';
                 $("#apellidos_div").append( $(''+textos) );


              }
              if(user=="Secretaria" || user=="Administrador" || user=="Profesor"){
                  /*No se le hace cambio al formulario*/

              }

            });
       });

</script>

<!--Script para guardar la información de cada usuario-->

<script type="text/javascript">
       $(document).ready(function (){
           $("#btnagregarUsuario").click(function(e){

                 var url = "<?php echo base_url().'index.php/con_admin'?>"+"/agregarUser";
                 var user = $("#tipo_usuario").val();
                 /*Inicializamos variables que tienen en comun los usuarios */
                 var data = $(this).serializeArray();

                 

                  console.log("paso");

                 if (user=="Alumno"){

                    var anio= $("#anio_").val();
                    var apellidos= $("#apellidos_").val();
                 
                    data.push({name:'anio',value:anio});
                    data.push({name:'apellidos',value:apellidos});
              
                 }
                 if(user=="Empresa"){

                    var permiso= "Empresa";
                 }
                 if(user=="Profesor" || user=="Secretaria" || user=="Administrador"){
                    var apellidos= $("#apellidos_").val();
                    
                    data.push({name:'apellidos',value:apellidos});
                  

                 }
                  
                 var rut = $("#rut_").val();
                 var nombres= $("#nombres_").val();                
                 var email= $("#email_").val();
                 var pass= $("#pass_").val();                 
                 var estado= $("#estado_").val();
                 var telefono= $("#telefono_").val();
                 var direccion = $("#direccion_").val();
                 var permiso= user;
                 data.push({name:'direccion',value:direccion});
                 data.push({name:'permiso',value:permiso});
                 data.push({name:'rut',value:rut});
                 data.push({name:'nombres',value:nombres});                
                 data.push({name:'email',value:email});
                 data.push({name:'pass',value:pass});
                 data.push({name:'estado',value:estado});
                 data.push({name:'telefono',value:telefono});
                 
                 console.log(data); 

                  $.ajax({                  
                            url: url,
                            type: "POST",
                          //  dataType: "JSON",
                            data: data,

                            success: function(data)  {
                      
                              
                             },

                            error: function(result) {
                            alert("Error al ingresar al usuario");
                            console.log("Error: " + result.statusText);
                            }
                           
                });

            });
     });

</script>

<!--

<script type="text/javascript">
       $(document).ready(function (){
          $("#noty").click(function(){
            var n = noty({
                    text: 'Se ha iniciado sesión exitosamente',
                    type: 'success',
                    layout: 'topCenter',
                    theme: 'relax',
                    timeout: '1000',
                    callback: {
                        afterClose: function (){
                          window.location = data.redirect;
                        }
                  }   
             });
        });
      });
</script>
-->


<script type="text/javascript" charset="utf-8"> 

      $(document).ready(function() {
          $('#example').dataTable();

          });
</script> 
<!--<script type="text/javascript" src="<?php echo base_url();?>noty-2.3/js/noty/packaged/jquery.noty.packaged.min.js"></script>



