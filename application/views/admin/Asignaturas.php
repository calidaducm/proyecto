<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Alumnos</h3> 
                   </div>
              <br> 
      

       <form>
              <table cellpadding="0" cellspacing="0" border="10" class="table table-bordered table-hover" id="tabla"  >
                  <thead> 
                    <tr>
                              <th>Codigo</th>
                              <th>Nombre</th>
                              <th>Semestre</th>
                              <th>Año</th>
                              <th>Créditos</th>
                              <th>Descripción</th>
                              <th>Opciones</th>
                             
                          </tr>
                  </thead> 
                  
                  <tbody> 
                       <?php foreach($query as $row):?>
                             <tr>
                                
                                 
                                 <td> <?=$row->Codigo?> </td> 
                                 <td> <?=$row->Nombre_ramo?> </td>
                                 <td> <?=$row->Semestre?> </td>
                                 <td> <?=$row->Año?> </td>
                                 <td> <?=$row->Creditos?> </td>
                                 <td> <?=$row->Descripcion?> </td>

                                  <td class="col-lg-4">
                                 <!-- Button trigger modal href="buscarUsuarioId/<?=$row->rut?>"-->
                                      <a href="<?=$row->Codigo?>" class="Editar">
                                         <button  type="button" class="btn btn-info " data-toggle="modal" data-target="#ModalEditarAsignatura" ><i class="glyphicon glyphicon-edit"></i> 
                                          Editar
                                         </button> 
                                     </a> 
                                      <!--<a href="<?php echo base_url().'index.php/con_admin'?>/EliminarAsignatura/<?=$row->Codigo?>" class="Eliminar">                               
                                         <button   type="button" class="btn btn-danger btn-sm" ><i class="glyphicon glyphicon-edit"></i> 
                                         Eliminar
                                         </button>
                                      </a>-->
                                   <!-- Modal -->
                                  
                                </td>
                             </tr> 
                        <?php endforeach;?> 
                   
                 </tbody>
           </table>
          </form>
      </div>
   </div>
</div>


<div class="modal fade" id="ModalInsertarAsigntura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel" align="center">Editar Asignaturas</h4>
          </div>
          <div class="modal-body">

               <form action="addAsignatura" method="POST" > 
                  <div align="center" > 
                      Código:
                      <input id="codigo_" type="text" name="codigo_" class="form-control" readonly="readonly"  disabled/>
                      Nombre:
                      <input id="nombre_" type="Text" name="nombre_" class="form-control" required/>
                      Semestre :
                      <input type="Text" name="semestre_" class="form-control" required />
                      Año :
                      <input type="Text" name="anio_" class="form-control"  />
                       Creditos :
                      <input type="Text" name="creditos_" class="form-control"  />
                       Descripcion :
                      <input type="Text" name="descripcion_" class="form-control"  />
                  </div>
                  
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                     <button type="submit" class="btn btn-primary">Guardar</button>
                 </div>
              </form>
           </div>
            
      </div>
    </div>
  </div>


<div class="modal fade" id="ModalEditarAsignatura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel" align="center">Editar Asignaturas</h4>
          </div>
          <div class="modal-body">

               <form action="UpdateAsignatura" method="POST" > 
                  <div align="center" > 
                      Código:
                      <input id="codigoedit_" type="text" name="codigo_" class="form-control" readonly="readonly"  />
                      Nombre:
                      <input id="nombreedit_" type="Text" name="nombre_" class="form-control" required/>
                      Semestre :
                      <input id="semestreedit_" type="Text" name="semestre_" class="form-control" required />
                      Año :
                      <input id="anioedit_" type="Text" name="anio_" class="form-control"  />
                       Creditos :
                      <input id ="creditosedit_"type="Text" name="creditos_" class="form-control"  />
                       Descripcion :
                      <input id="descripcionedit_" type="Text" name="descripcion_" class="form-control"  />
                  </div>
                  
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                     <button type="submit" class="btn btn-primary">Guardar</button>
                 </div>
              </form>
           </div>
            
      </div>
    </div>
  </div>

<div class="modal fade" id="ModalEliminarAsignatura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel" align="center">Editar Asignaturas</h4>
          </div>
          <div class="modal-body">

               <form action="EliminarAsignatura" method="POST" > 
                  <div align="center" > 
                       <div class="alert alert-warning">
                          <a href="#" class="alert-link">Seguro que desea eliminae</a>
                        </div>
                  </div>
                  
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                     <button type="submit" class="btn btn-primary">Eliminar</button>
                 </div>
              </form>
           </div>
            
      </div>
    </div>
  </div>




<script type="text/javascript">
       $(document).ready(function (){
           $("a").click(function(e){
           
             

              if ($(this).attr("class")=="Editar")  {
                  e.preventDefault();                         //Evita que el evento se genere (redireccionar)
                  var url = "<?php echo base_url().'index.php/con_admin'?>"+"/EditarAsignatura/";
                  var link = $(this).attr("href");          // se obtiene el valor del atributo href de la etiqueta "a"
                          
                 //location.href(text);                      //Redirige a un link
                  $.ajax({
              
                        url: url+link,
                        type: "POST",
                        dataType : 'JSON',

                        success: function(data)  {
                           console.log(data);
                           $('#codigoedit_').val(data.Codigo);
                           $('#nombreedit_').val(data.Nombre_ramo);
                           $('#semestreedit_').val(data.Semestre);
                           $('#anioedit_').val(data.Anio);
                           $('#creditosedit_').val(data.Creditos);
                           $('#descripcionedit_').val(data.Descripcion);

                          // $('#')
                        },
                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });
               } 
             if ($(this).attr("class")=="Eliminar"){
                  var answer = confirm("¿Seguro que desea eliminar la asignatura?","si");
                  if (answer){
                      
                  }
                  else{
                    e.preventDefault();  
                      alert("Bye bye!")
                    alert("Thanks for sticking around!")
                  }
                

             }     
            });
       });
</script> 