<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content=""
    <link rel="icon" href="../../favicon.ico">
  
    <!-- CARGA DE BOOTSTRAP -->
    <link href="<?php echo base_url();?>bootstrap3.2/dist/css/bootstrap.css"  rel="stylesheet" type="text/css" media="all" />
    
    <link  href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" rel="stylesheet"></link>
    
    <!-- CARGA JQUERY-->
    <script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo base_url();?>bootstrap3.2/dist/js/bootstrap.min.js"  ></script>
    
    <script src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>  
    

    <!--<link rel="stylesheet" href="http://localhost/auditoria/mallajs/custom.css">-->
    <!--- <script src="<?php echo base_url();?>js/datatable/jquery.dataTables.js"></script>     PLUGIN DATATABLE-->
    <script src="<?php echo base_url();?>DataTables-1.10/media/js/jquery.dataTables.js"></script>      <!--PLUGIN DATATABLE-->

    <script type="text/javascript" src="<?php echo base_url();?>validator.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>noty-2.3/js/noty/jquery.noty.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>noty-2.3/js/noty/packaged/jquery.noty.packaged.min.js"></script>
   

    <title>Inicio Administrador</title>

    <div id="tablaMalla_filter" class="dataTables_filter"><label>Buscar:<input type="search" class="" placeholder="" aria-controls="tablaMalla"></label></div>



  </head>
  
    <nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span >Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Bienvenido</a>
        </div>
 
        <div id="navbarCollapse" class="collapse navbar-collapse ">
            <ul class="nav navbar-nav ">
             
              <li><a  <?php echo anchor("con_admin/gestionUser","Usuarios")?> </a></li>
              <li><a  <?php echo anchor("con_admin/gestionAsignaturas","Asignaturas")?> </a></li>
              <li><a  <?php echo anchor("con_admin/visualizarAlumnos","Visualizar alumno")?> </a> </li>
             <!-- <li><a  <?php echo anchor("con_admin/historial","Historial")?></li>-->
            
              <li><a  href="<?php echo base_url();?>index.php/Reportes">Reportes </a> </li>
             
              <li class="dropdown ">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">Habilitar Alumnos <b class="caret"></b></a>
                  <ul role="menu" class="dropdown-menu">
                      <li><a  <?php echo anchor("con_admin/PracticaTemprana","Practicas")?> </a> </li>
                  

                  </ul>
              </li>
               <li><a  <?php echo anchor("con_admin/Practicantes","Alumnos en Práctica")?> </a> </li>
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#" >Configuración <b class="caret"></b></a>
                  <ul role="menu" class="dropdown-menu" style="padding-right:20px">
                        
                         <!--<li> <a>Modificar tus datos </a></li>-->
                         <li class="" ><a  <?php echo anchor("cambiar_clave","Cambiar Password")?> </a> </li>
                         
                         <li class="pull-left" ><a <?php echo anchor("con_cerrar_sesion","Cerrar Sesion")?> </a>   </li>
                 
                </ul>
              </li>             
              </ul>
      <div>           
   </nav>




 
  <body style="background-image:url(<?php echo base_url();?>2.jpg);">