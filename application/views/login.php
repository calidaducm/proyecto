
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Gesti&oacuten de Practicas</title>

<!-- CARGA DE BOOTSTRAP -->
    <link href="<?php echo base_url();?>bootstrap3.2/dist/css/bootstrap.css"  rel="stylesheet" type="text/css" media="all" />
   
    <!-- BootstrapValidator CSS -->
    <link href="<?php echo base_url();?>Bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/>
    <!-- CARGA JQUERY-->
    <script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo base_url();?>bootstrap3.2/dist/js/bootstrap.min.js"  ></script>
  

    <!-- BootstrapValidator JS-->
    <script src="<?php echo base_url();?>noty-2.3/js/noty/packaged/jquery.noty.packaged.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>Bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
   
    
    <!--<link rel="stylesheet" href="http://localhost/auditoria/mallajs/custom.css">
    <script src="<?php echo base_url();?>js/datatable/jquery.dataTables.js"></script> -->    <!--PLUGIN DATATABLE-->
   
  </head>

 
  <body style="background-image:url('../login.jpg');">


    <div class="container">
      <?php echo form_open('logear/verificar');?>
      <form id="registration-form" action="logear/verificar" method="POST" class="form-horizontal" >
           <div class="form-group">
              </br></br>
               <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                  <h3  style="margin-bottom:10px;" class="form-signin-heading text-left">Ingreso</h3>
                  <input style="margin-bottom:10px;" type="text" id="rut_id" name="rut_id" class="form-control" placeholder="Rut, sin guión y sin dígito verificador" /> 
                  <span class="help-block"></span>
                  <input style="margin-bottom:10px;" type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" />
                      <button id="ingresar" class="btn btn-lg btn-success btn-block" type="submit">
                        Ingresar</button>
                </div>
               </div>
            </div> 
       </form>
        <?php echo form_close();?>    
    </div>
<!--
                     
<script type="text/javascript">
       $(document).ready(function (){
          $("#ingresar").click(function(){
            var n = noty({ 

                   // if ($(""))
                    text: 'Error en el inicio de sesiòn',
                    type: 'error',
                    layout: 'top',
                    theme: 'relax',
                    timeout: '2000',
                  /* 
                    animation: {
                      open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                        close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                        easing: 'swing',
                        speed: 2000 // opening & closing animation speed
                    },
                    callback: {
                        afterClose: function (){
                        window.location = 'http://localhost/Auditoria/index.php/con_admin/visualizarAlumnos'
                        }
                    },  
                  */
                        maxVisible: 5, // you can set max visible notification for dismissQueue true option,
                        killer: false, // for close all notifications before show
                        closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
                        callback: {
                              onShow: function() {}, //Funciòn al mostrar el evento
                              afterShow: function() {}, //Codigo que se ejecuta despuès que se empieza a salir la animacion
                              onClose: function() {},   //Codigo que se ejecuta despues de que se empiece a cerrar la salida
                              afterClose: function() {}, //Codigo que se ejecuta despues de que se cierre la animacion
                              onCloseClick: function() {},//¿?¿
                        }, 
                 });
            });
         });
</script>
-->


<script>
$(document).on("ready",inicio);

function inicio(){
  $("span.help-block").hide();
  $("#ingresar").click(function(){
    if(validar()==false)
      //alert("los campos no estan validados");
    else{
      //alert("los campos estan validados");
    }
  });
  $("#rut_id").keyup(validar);
}

function validar(){
  var valor = $("#rut_id").val();
  if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
    $("#iconotexto").remove();
    $("#rut_id").parent().parent().attr("class","form-group has-error has-feedback");
    $("#rut_id").parent().children("span").text("Debe ingresar algun caracter").show();
    $("#rut_id").parent().append("<span id='iconotexto' class='glyphicon glyphicon-remove form-control-feedback'></span>");
      return false;
  }
  else if( isNaN(valor) ) {
    $("#iconotexto").remove();
    $("#rut_id").parent().parent().attr("class","form-group has-error has-feedback");
    $("#rut_id").parent().children("span").text("Debe ingresar caracteres numericos").show();
    $("#rut_id").parent().append("<span id='iconotexto' class='glyphicon glyphicon-remove form-control-feedback'></span>");
    return false;
  }
  else{
    $("#iconotexto").remove();
    $("#rut_id").parent().parent().attr("class","form-group has-success has-feedback");
    $("#rut_id").parent().children("span").text("").hide();
    $("#rut_id").parent().append("<span id='iconotexto' class='glyphicon glyphicon-ok form-control-feedback'></span>");
    return true;
  }
}
</script>
  </body>
</html>