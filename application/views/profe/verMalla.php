<div class="container" style="padding-top: 80px;">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Alumnos</h3> 
                   </div>
                      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="tablaMalla">
                          <thead> 
                                  <tr>
                                      <th>Rut</th>
                                      <th>Nombres</th>
                                      <th>Apellidos</th>
                                      <th>Opción</th>
                                 </tr>
                          </thead> 
                          
                          <tbody> 
                               <?php
                               foreach($alumnos as $row):?>

                                     <tr>
                                         <td><?=$row->Rut?></td>
                                         <td><?=$row->Nombres?></td>
                                         <td><?=$row->Apellidos?></td>
                                         <td>
                                                                                  
                                          <a href="<?=$row->Rut?>" class="AchurarMalla">
                                          <button type="button" class="btn btn-info btn-sm" >
                                              <i class="glyphicon glyphicon-eye-open"></i> 
                                                 Mostrar Malla
                                          </button>
                                          </a>

                                          </td>  
                                         
                                    </tr> 
                                   
                               <?php endforeach;?> 
                         </tbody>
                   </table>
              </div>           
          </div>
       </div> 
    </div>
</div>
 
  
  <div class="container">
     <div class="row">
           <div class="panel panel-primary">
                  <div class="panel-heading">
                   <h3 class="panel-title">Malla curricular</h3>
                   </div>         
          <div class="panel-body">
        <form disabled="disabled">
           <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover"  >
              
                    <thead> 
                              <tr>
                                  <th colspan="2">Primer año</th>
                                  <th colspan="2">Segundo año</th>
                                  <th colspan="2">Tercer año</th>
                                  <th colspan="2">Cuarto año</th>
                                  <th colspan="2">Quinto año</th>
                                 
                              </tr>
                      </thead> 
                      
                      <tbody> 
 
                       <?php for ($j=1; $j <= 5; $j++) { ?>      

                          <tr>
                                <?php for ($i=0; $i <=9 ; $i++) { ?>
                                        <td disabled >                     
                                            <a href="<?=$j+5*$i?>"  >
                                               <div id= "<?=$j+5*$i?>"  class="ramo">
                                                        <div class="" align="center">
                                                            <label>
                                                            <input   style="width:95px; font-size: 8pt"  id= "Codigo<?=$j+5*$i?>" name ="Codigo<?=$j+5*$i?>" disabled> </input><br>
                                                            <input   style="width:95px; font-size: 8pt"  id= "Estado<?=$j+5*$i?>" name ="Estado<?=$j+5*$i?>" disabled></input>
                                                            </label>
                                                       </div> 
                                              </div>
                                             </a>                                    
                                        </td> 
                                <?php }; ?> 
                            </tr>
                        <?php } ?>
                                   
                    </tbody>
               </table>
                  <!--  <button type="submit" class="btn btn-info btn-sl " align="center">Finalizar Achurado</button>
                    <input id="btnAchurarMalla" name="RutAlumno" type="hidden" ></input>-->
                 </form>    
             </div>
          </div>
      </div>
    </div>
           

<script type="text/javascript">
       $(document).ready(function (){
          

           $("td").click(function(e){
              e.preventDefault(); 
              e.stopPropagation();
            });



           $("a").click(function(e){
                

              if ($(this).attr("class")=="AchurarMalla")  {
                 
                  e.preventDefault();                        //detiene el redirect.
                 
                  var id = $(this).attr("href");   
                  var url = "<?php echo base_url().'index.php/con_admin'?>"+"/MallaAchurada/"+id;
                         // se obtiene el valor del atributo href de la etiqueta "a"
              
                //  $("#btnAchurarMalla").val(id); 
                  $.ajax({
              
                        url: url,
                        type: "POST",
                        dataType : 'JSON',

                        success: function(data)  {
                           //Obtenemos los valores de los ramos del alumno, si esta aprobado o no.
                         // console.log(data);
                          var aux="";
                           for (var i = 0 ; i<= 49; i++) {
                                                                   
                                aux=data.resultado[i].Codigo_asignatura;
                                $('#Codigo'+aux).val(data.resultado[i].Nombre_ramo);
                                
                                if (data.resultado[i].Estado =="No aprobado") {
                                     $('#Estado'+aux).val("No aprobado");
                                     $('#'+aux).css({ 'opacity' : 1});
                                     $('#'+aux).css({ 'background-color': blue});
                                     $('#'+aux).attr("class","ramo");
 
                                     
                                 }
                                if (data.resultado[i].Estado =="Aprobado") {
                                     $('#Estado'+aux).val("Aprobado");
                                     $('#'+aux).css({ 'opacity' : 0.4 });
                                     $('#'+aux).attr("class","0");

                                }
                                else{

                                     $('#Estado'+aux).val(data.resultado[i].Estado);                                  
                                }

                         
                            }

                        },

                        error: function(result) {
                        console.log("Error" + result);
                        }
                        });
                }
             
          });

       });
</script>









<script type="text/javascript" charset="utf-8"> 

      $(document).ready(function() {
          $('#tablaMalla').dataTable({
          
            "language": {
              "sProcessing":    "Procesando...",
              "sLengthMenu":    "Mostrar _MENU_ registros",
              "sZeroRecords":   "No se encontraron resultados",
              "sEmptyTable":    "Ningún dato disponible en esta tabla",
              "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":   "",
              "sSearch":        "Buscar:",
              "sUrl":           "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":    "Último",
                  "sNext":    "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            }
          });
          });
</script> 