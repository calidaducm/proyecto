$(document).ready(function() {
	$('#login').bootstrapValidator({
		 message: 'Este valor no es valido',
		 feedbackIcons: {
			 valid: 'glyphicon glyphicon-ok',
			 invalid: 'glyphicon glyphicon-remove',
			 validating: 'glyphicon glyphicon-refresh'
		 },
		 fields: {
			 rut_id: {
				 validators: {
					 notEmpty: {
						 message: 'El nombre de usuario es requerido, error!'
					 }
				 }
			 },
			 inputPassword: {
				 validators: {
					 notEmpty: {
						 message: 'La contraseña es requerida'
					 }
				 }
			 }
		 }
		});
});